import os
import jupytext
import papermill as pm
import logging
from pathlib import Path

log = logging.getLogger("convergence_reports")
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)
log.addHandler(sh)

proj_dir = Path(os.path.dirname(os.path.dirname(__file__)))
nb_dir = proj_dir / "notebooks"
reports_dir = proj_dir / "notebooks" / "reports"


def generate_report(notebook_name: str, params: dict):
    ### jupytext : sync md version to ipynb
    nb_file = nb_dir / notebook_name
    nb_file_md = nb_file.with_suffix(".md")
    nb_file_ipynb = nb_file.with_suffix(".ipynb")
    nb_file_html = nb_file.with_suffix(".html")
    nb_file_report = (
        reports_dir / (notebook_name + "_" + params["model_id"])
    ).with_suffix(".html")
    nb = jupytext.read(nb_file_md)
    jupytext.write(nb, nb_file_ipynb, fmt="ipynb")
    ### Execute parametrized notebook
    params["nb_dir"] = str(nb_dir)
    pm.execute_notebook(
        nb_file_ipynb, nb_file_ipynb, parameters=params, log_output=True
    )
    ### Convert and save notebooks to html
    conv_cmd = "jupyter nbconvert --to html {}".format(str(nb_file_ipynb))
    nbconv_out = os.popen(conv_cmd).read()
    log.info(nbconv_out)
    ### Move coverted notebook to reports folder
    os.replace(nb_file_html, nb_file_report)


if __name__ == "__main__":
    run_inference = True
    idata_dir = os.path.join(proj_dir, "data", "inference", "local")
    nb_list = [
        {
            "name": "building_signature_inference",
            "params": {
                "model_id": "types",
                "run_inference": run_inference,
            },
        },
        {
            "name": "building_signature_inference",
            "params": {
                "model_id": "surfaces",
                "run_inference": run_inference,
            },
        },
        {
            "name": "building_signature_inference",
            "params": {
                "model_id": "dates",
                "run_inference": run_inference,
            },
        },
    ]
    for nb in nb_list:
        log.info("Generating convergence report for model " + nb["name"])
        generate_report(notebook_name=nb["name"], params=nb["params"])
        log.info("Report generated for model " + nb["name"])
