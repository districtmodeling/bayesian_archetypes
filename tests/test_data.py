# -*- coding: utf-8 -*-

import pytest
from pandas.testing import assert_frame_equal
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp

__author__ = "Nils Artiges"
__copyright__ = "Nils Artiges"
__license__ = "apache"


@pytest.fixture()
def data():
    data = io.Data()
    data.data_raw_folder = "./data/raw/"
    return data


@pytest.fixture()
def iris(data):
    iris = cp.Iris(data=data)
    iris.preprocessed_data = "./data/preprocessed/iris_dataframes.h5"
    return iris


class TestIO:
    def test_data_instantiation(self, data):
        assert isinstance(data, io.Data)


class TestConsistency:
    def test_ratio_values(self, iris):
        thermo_df = iris.thermo_df
        rt_cols = ["ratio_house", "ratio_apartment"]
        rd_cols = [col for col in thermo_df.columns if "ratio_date" in col]
        rs_cols = [col for col in thermo_df.columns if "ratio_surf" in col]

        def sum_of_ratios_equals_one(cols_list):
            test_df = (thermo_df[cols_list].sum(axis=1) > 0.99) & (
                thermo_df[cols_list].sum(axis=1) < 1.01
            )
            return test_df.mean() > 0.99

        assert sum_of_ratios_equals_one(rt_cols)
        assert sum_of_ratios_equals_one(rd_cols)
        assert sum_of_ratios_equals_one(rs_cols)

    def test_thermo_subsets_df_similar_with_original_thermo_df(self, iris):
        df_init = iris.thermo_df
        df = iris.thermo_subsets_df
        # values
        df_v = df.groupby("iris_code")[
            "enedis_sites_number",
            "thermosensitive_consumption",
            "total_consumption",
            "base_consumption",
            "hlc",
            "dwellings_number",
        ].sum()
        # types and dates
        df_d = df.groupby(["iris_code", "dwelling_type", "dwelling_date"]).sum()
        df_d = df_d.pivot_table(
            columns=["dwelling_type", "dwelling_date"],
            index="iris_code",
            values="dwellings_number",
        )
        new_cols = [
            "ratio_date_" + i[0] + "_" + i[1] for i in df_d.columns.to_flat_index()
        ]
        df_d.columns = new_cols
        # surfaces
        df_s = df.groupby(["iris_code", "dwelling_surf"]).sum()
        df_s = df_s.pivot_table(
            columns=["dwelling_surf"], index="iris_code", values="dwellings_number"
        )
        new_cols = ["ratio_surf_" + i for i in df_s.columns]
        df_s.columns = new_cols
        # types
        df_t = df.groupby(["iris_code", "dwelling_type"]).sum()
        df_t = df_t.pivot_table(
            columns=["dwelling_type"], index="iris_code", values="dwellings_number"
        )
        new_cols = ["ratio_" + i for i in df_t.columns]
        df_t.columns = new_cols
        # concatenate to reconstruct df_init
        df = df_v.join(df_t)
        df = df.join(df_s)
        df = df.join(df_d)
        df.index.name = None
        # recompute ratios
        for c in df.columns:
            if "ratio_" in c:
                df[c] = df[c] / df["dwellings_number"]
        df_init.drop(columns="hlc_avg", inplace=True)
        # assert equality between dataframes
        assert_frame_equal(
            df_init.reindex(columns=df_v.columns),
            df.reindex(columns=df_v.columns),
            check_dtype=False,
        )
