"""
Module for data processing.
"""

__version__ = "0.1.0"
__author__ = "Nils Artiges"
__licence__ = "Apache"

from bayesian_archetypes.io import Data, Weather, proj_dir
from tqdm import tqdm
from scipy.interpolate import griddata
from scipy.optimize import root_scalar
import pandas as pd
import geopandas as gp
import numpy as np
import os


class Iris:
    """Class for IRIS data processing."""

    def __init__(self, data: Data) -> None:
        """Instantiation of Iris class.

        Args:
            data (Data): Data object.
        """
        self.__data = data
        self.__weather = Weather(data=data)
        self.__enedis_insee_df = pd.DataFrame()
        self.__thermo_df = pd.DataFrame()
        self.__thermo_subsets_df = pd.DataFrame()
        self.__borders_df = gp.GeoDataFrame()
        self.__temperatures_df = pd.DataFrame()
        self.preprocessed_data = os.path.join(
            proj_dir, "data", "preprocessed", "iris_dataframes.h5"
        )
        self.preprocessed_dir = os.path.join(proj_dir, "data", "preprocessed")
        pass

    def get_enedis_insee_df(self) -> pd.DataFrame:
        """Returns simple fusion of ENEDIS and INSEE dataframes.

        Returns:
            pd.DataFrame: Fused dataframe from INSEE and ENEDIS data.
        """
        if self.__enedis_insee_df.empty:
            df_enedis = self.__data.enedis_df()
            df_insee = self.__data.insee_df()

            # pre-process dataproperty
            df = pd.concat(
                [
                    df_insee.set_index("IRIS", drop=True),
                    df_enedis.set_index("Code IRIS"),
                ],
                axis=1,
            )
            df.dropna(
                axis=0,
                subset=[
                    "P16_RP",
                    "Nb sites Résidentiel",
                    "Conso totale Résidentiel usages thermosensibles (MWh)",
                ],
                inplace=True,
            )
            self.__enedis_insee_df = df
        return self.__enedis_insee_df

    @property
    def thermo_df(self) -> pd.DataFrame:
        """Returns a curated dataframe for iris consumptions and dwellings statistics

        Returns:
            pd.DataFrame: Dataframe with iris thermo-energetic data with dwellings statistics.
                          The dataframe is cleaned from unnecessary data, renamed to more understandable columns,
                          and ratios sums normalized to 1.
        """
        if os.path.isfile(self.preprocessed_data) and self.__thermo_df.empty:
            self.__thermo_df = pd.read_hdf(
                self.preprocessed_data, key="iris_thermo_data"
            )
        elif self.__thermo_df.empty:
            # initialize data
            columns_aliases = {
                "Nb sites Résidentiel": "enedis_sites_number",
                "Conso totale Résidentiel "
                "usages thermosensibles (MWh)": "thermosensitive_consumption",
                "Conso totale Résidentiel (MWh)": "total_consumption",
                "Conso totale Résidentiel "
                "usages non thermosensibles (MWh)": "base_consumption",
                "Thermosensibilité totale Résidentiel (kWh/DJU)": "hlc",
                "DJU": "degree_day",
                "P16_RP": "dwellings_number",
                "P16_RPMAISON": "ratio_house",
                "P16_RPAPPART": "ratio_apartment",
                "P16_RP_M30M2": "ratio_surf_inf_30",
                "P16_RP_3040M2": "ratio_surf_30_40",
                "P16_RP_4060M2": "ratio_surf_40_60",
                "P16_RP_6080M2": "ratio_surf_60_80",
                "P16_RP_80100M2": "ratio_surf_80_100",
                "P16_RP_100120M2": "ratio_surf_100_120",
                "P16_RP_120M2P": "ratio_surf_120_inf",
                "P16_RPMAISON_ACH19": "ratio_date_house_inf_1919",
                "P16_RPMAISON_ACH45": "ratio_date_house_1919_1945",
                "P16_RPMAISON_ACH70": "ratio_date_house_1945_1970",
                "P16_RPMAISON_ACH90": "ratio_date_house_1970_1990",
                "P16_RPMAISON_ACH05": "ratio_date_house_1990_2005",
                "P16_RPMAISON_ACH13": "ratio_date_house_2005_2013",
                "P16_RPAPPART_ACH19": "ratio_date_apartment_inf_1919",
                "P16_RPAPPART_ACH45": "ratio_date_apartment_1919_1945",
                "P16_RPAPPART_ACH70": "ratio_date_apartment_1945_1970",
                "P16_RPAPPART_ACH90": "ratio_date_apartment_1970_1990",
                "P16_RPAPPART_ACH05": "ratio_date_apartment_1990_2005",
                "P16_RPAPPART_ACH13": "ratio_date_apartment_2005_2013",
            }
            iris_thermo_data = self.get_enedis_insee_df()
            # correct inconsistencies in dwellings counts
            iris_thermo_data["P16_RP"] = (
                iris_thermo_data["P16_RPMAISON"] + iris_thermo_data["P16_RPAPPART"]
            )
            # compute missing range counts (upper date ranges)
            iris_thermo_data["ratio_date_house_2013_inf"] = (
                iris_thermo_data["P16_RPMAISON"]
                - iris_thermo_data["P16_RPMAISON_ACHTOT"]
            )
            iris_thermo_data["ratio_date_apartment_2013_inf"] = (
                iris_thermo_data["P16_RPAPPART"]
                - iris_thermo_data["P16_RPAPPART_ACHTOT"]
            )
            iris_thermo_data = iris_thermo_data.rename(columns=columns_aliases)
            # re-normalize ratios
            type_cols = ["ratio_house", "ratio_apartment"]
            surf_cols = [col for col in iris_thermo_data.columns if "ratio_surf" in col]
            date_cols = [col for col in iris_thermo_data.columns if "ratio_date" in col]
            type_counts = iris_thermo_data[type_cols].sum(axis=1)
            surf_counts = iris_thermo_data[surf_cols].sum(axis=1)
            date_counts = iris_thermo_data[date_cols].sum(axis=1)
            total_counts = iris_thermo_data["dwellings_number"]
            for c in type_cols:
                iris_thermo_data[c] = iris_thermo_data[c] / type_counts
            for c in surf_cols:
                iris_thermo_data[c] = iris_thermo_data[c] / surf_counts
            for c in date_cols:
                iris_thermo_data[c] = iris_thermo_data[c] / date_counts
            # cleanup
            iris_thermo_data = iris_thermo_data[
                [col for key, col in columns_aliases.items()]
                + ["ratio_date_house_2013_inf", "ratio_date_apartment_2013_inf"]
            ]
            # average of hlc and base consumption
            iris_thermo_data["hlc_avg"] = iris_thermo_data["hlc"] / total_counts
            iris_thermo_data["base_consumption_avg"] = (
                iris_thermo_data["base_consumption"] / total_counts
            )
            # compute Ts temperatures (thermo sensitivity setpoints)
            def dju_func(temperatures, dju_iris):
                def _dju(ts):
                    t_day = temperatures.resample("1D").mean()
                    return (ts - t_day[t_day < ts]).sum() - dju_iris

                return _dju

            df_ts = pd.DataFrame()
            for iris_code, temp in tqdm(
                self.temperatures_df.iteritems(), total=iris_thermo_data.shape[1]
            ):
                DJU = iris_thermo_data.loc[iris_code, "degree_day"]
                r = root_scalar(dju_func(temp, DJU), method="secant", x0=0, x1=20)
                df_ts = df_ts.append(
                    {"iris_code": iris_code, "Ts": r.root}, ignore_index=True
                )
            iris_thermo_data = iris_thermo_data.join(df_ts.set_index("iris_code"))
            # save data to preprocessed data folder
            self.__thermo_df = iris_thermo_data
            self.__thermo_df.to_hdf(self.preprocessed_data, key="iris_thermo_data")
        return self.__thermo_df

    @property
    def type_ratios(self) -> pd.DataFrame:
        """Returns ratios for dwellings types (house and apartments).

        Returns:
            pd.DataFrame: ratios for dwellings types (house and apartments).
        """
        type_cols = ["ratio_house", "ratio_apartment"]
        return self.thermo_df[type_cols]

    @property
    def date_ratios(self) -> pd.DataFrame:
        """Returns ratios for type and date categories.

        Returns:
            pd.DataFrame: ratios for type and date categories.
        """
        date_cols = [
            "ratio_date_house_inf_1919",
            "ratio_date_house_1919_1945",
            "ratio_date_house_1945_1970",
            "ratio_date_house_1970_1990",
            "ratio_date_house_1990_2005",
            "ratio_date_house_2005_2013",
            "ratio_date_house_2013_inf",
            "ratio_date_apartment_inf_1919",
            "ratio_date_apartment_1919_1945",
            "ratio_date_apartment_1945_1970",
            "ratio_date_apartment_1970_1990",
            "ratio_date_apartment_1990_2005",
            "ratio_date_apartment_2005_2013",
            "ratio_date_apartment_2013_inf",
        ]
        return self.thermo_df[date_cols]

    @property
    def surface_ratios(self) -> pd.DataFrame:
        """Returns ratios for surface categories.

        Returns:
            pd.DataFrame: ratios for surface categories.
        """
        surf_cols = [
            "ratio_surf_inf_30",
            "ratio_surf_30_40",
            "ratio_surf_40_60",
            "ratio_surf_60_80",
            "ratio_surf_80_100",
            "ratio_surf_100_120",
            "ratio_surf_120_inf",
        ]
        return self.thermo_df[surf_cols]

    @property
    def product_ratios(self) -> pd.DataFrame:
        """Returns ratios of sub-categories extrapolated from products of marginal categories.

        Returns:
            pd.DataFrame: ratios of sub-categories extrapolated from products of marginal categories.
        """
        r_date = self.date_ratios
        r_surf = self.surface_ratios
        ratios = np.repeat(r_date.values, 7, axis=1) * np.tile(r_surf.values, 14)
        r_names = []
        for d in r_date.columns:
            for s in r_surf.columns:
                r_names.append(d + ":" + s.split("ratio_")[1])
        return pd.DataFrame(data=ratios, columns=r_names, index=r_date.index)

    @property
    def joint_ratios(self) -> pd.DataFrame:
        """Return ratios of sub categories estimated externally.

        Returns:
            pd.DataFrame: ratios of sub categories estimated externally.
        """
        df = pd.read_csv(
            os.path.join(self.preprocessed_dir, "joint_ratios.csv"), index_col=0
        )
        return df

    @property
    def thermo_subsets_df(self) -> pd.DataFrame:
        """Returns thermo energy data naively subdivided per dwelling category and IRIS
          (ie. aggregate values divided by estimated number of dwelling per category.)

        Returns:
            pd.DataFrame: thermo energy data naively subdivided per dwelling category.
        """
        if self.__thermo_subsets_df.empty:
            # prepare data
            df = self.thermo_df.drop(
                columns=["ratio_house", "ratio_apartment"]
            ).reset_index()
            df["iris_code"] = df["index"].astype("int32")
            df.drop(columns="index", inplace=True)
            rd_cols = [col for col in df.columns if "ratio_date" in col]
            rs_cols = [col for col in df.columns if "ratio_surf" in col]
            i_cols = [col for col in df.columns if "ratio_" not in col]
            v_cols = [
                "thermosensitive_consumption",
                "total_consumption",
                "base_consumption",
                "hlc",
                "dwellings_number",
                "enedis_sites_number",
            ]
            df = df.melt(
                id_vars=i_cols + rs_cols,
                value_vars=rd_cols,
                var_name="date_type_cat",
                value_name="date_type_ratio",
            )
            df = df.melt(
                id_vars=i_cols + ["date_type_cat", "date_type_ratio"],
                value_vars=rs_cols,
                var_name="surf_cat",
                value_name="surf_ratio",
            )
            # recompute data per subset
            df["ratio"] = df["date_type_ratio"] * df["surf_ratio"]
            for col in v_cols:
                df[col] = (df[col] * df["ratio"]).astype("float32")
            # filter and organize
            df = df[df["dwellings_number"] > 0.0]
            df.insert(0, "dwellings_number", df.pop("dwellings_number"))
            df.insert(0, "iris_code", df.pop("iris_code"))
            # create categories
            df["dwelling_type"] = (
                df["date_type_cat"]
                .apply(lambda x: "house" if "house" in x else "apartment")
                .astype("category")
            )
            df["dwelling_date"] = (
                df["date_type_cat"]
                .apply(lambda x: x.split("_")[-2] + "_" + x.split("_")[-1])
                .astype("category")
            )
            df["dwelling_surf"] = (
                df["surf_cat"]
                .apply(lambda x: x.split("_")[-2] + "_" + x.split("_")[-1])
                .astype("category")
            )
            # remove useless columns
            df.drop(
                columns=[
                    "hlc_avg",
                    "ratio",
                    "date_type_cat",
                    "date_type_ratio",
                    "surf_cat",
                    "surf_ratio",
                ],
                inplace=True,
            )
            self.__thermo_subsets_df = df
        return self.__thermo_subsets_df

    @property
    def averaged_df(self) -> pd.DataFrame:
        """Returns dataframe with average surface, dates and thermo-energetic values per IRIS.

        Returns:
            pd.DataFrame: dataframe with average surface, dates and thermo-energetic values per IRIS.
        """
        df = self.thermo_df.copy()
        surf_cols = [col for col in df.columns if "surf" in col]
        date_cols = [col for col in df.columns if "date_house" in col] + [
            col for col in df.columns if "date_apart" in col
        ]
        df["surf_avg"] = (df[surf_cols] * np.array([15, 35, 50, 70, 90, 110, 150])).sum(
            axis=1
        )
        df["date_avg"] = (
            df[date_cols]
            * np.array(
                [
                    1900,
                    1932,
                    1957.5,
                    1980,
                    1997.5,
                    2009,
                    2015,
                    1900,
                    1932,
                    1957.5,
                    1980,
                    1997.5,
                    2009,
                    2015,
                ]
            )
        ).sum(axis=1)
        df["hlc_avg_surf"] = df["hlc_avg"] / df["surf_avg"]
        return df[["ratio_house", "surf_avg", "date_avg", "hlc_avg", "hlc_avg_surf"]]

    @property
    def borders_df(self) -> gp.GeoDataFrame:
        """Returns a series of boundary polygons for each iris code

        Returns:
            gp.GeoDataFrame: series of boundary polygons for each iris code.
        """
        if self.__borders_df.empty:
            rename_dict = {
                "NOM_IRIS": "iris_name",
                "TYP_IRIS": "iris_type",
                "INSEE_COM": "postal_code",
            }
            shp_df = self.__data.iris_borders_df()
            shp_df.rename(columns=rename_dict, inplace=True)
            shp_df = shp_df.drop(columns=["NOM_COM", "IRIS"]).set_index("CODE_IRIS")
            shp_df["departement"] = shp_df["postal_code"].apply(lambda x: int(x[:2]))
            shp_df["postal_code"] = shp_df["postal_code"].astype("str")
            shp_df = shp_df.loc[shp_df.index.isin(self.get_enedis_insee_df().index)]
            self.__borders_df = shp_df
        return self.__borders_df

    def plot_treated_iris(self):
        """Plot treated IRISes on a map."""
        dfplot = self.borders_df  # .dissolve(by="departement")
        dfplot.simplify(tolerance=1000).plot(figsize=(8, 8))
        return

    @property
    def temperatures_df(self) -> pd.DataFrame:
        """Computes and returns time dataframe of IRIS temperatures,
           extrapolated with cubic interpolation on convex-hull and nearest method otherwise,
           from weather station data.

        Returns:
            pd.DataFrame: IRIS temperatures dataframe.
        """
        if os.path.isfile(self.preprocessed_data) and self.__temperatures_df.empty:
            self.__temperatures_df = pd.read_hdf(
                self.preprocessed_data, key="iris_temperatures_data"
            )
        elif self.__temperatures_df.empty:
            # build and save temperature df for all iris
            temp_metro = self.__weather.temperatures_metro_celsius
            temp_days = (
                temp_metro[["numer_sta", "t"]]
                .groupby(["numer_sta"])
                .resample("D")
                .mean()
                .interpolate(axis=0)
                .drop(columns="numer_sta")
                .reset_index()
                .set_index("time")
            )
            time = temp_days.index.unique()
            sta_x = [point.x for point in temp_metro.geometry.unique()]
            sta_y = [point.y for point in temp_metro.geometry.unique()]
            sta_points = np.transpose(np.array([sta_x, sta_y]))
            iris_centers = self.borders_df.centroid
            iris_codes = self.borders_df.index.tolist()

            temp_field = []
            for ts in tqdm(time):
                # get temperature values at station points
                values = temp_days.loc[ts].t.values
                # iris interpolation
                t_iris_quad = griddata(
                    sta_points, values, (iris_centers.x, iris_centers.y), method="cubic"
                )
                na = np.isnan(t_iris_quad)
                t_iris_near = griddata(
                    (iris_centers.x[~na], iris_centers.y[~na]),
                    t_iris_quad[~na],
                    (iris_centers.x, iris_centers.y),
                    method="nearest",
                )
                temp_field.append(t_iris_near)
            temp_field = np.asarray(temp_field)
            iris_temperatures_df = pd.DataFrame(
                data=temp_field, columns=iris_codes, index=time
            )
            self.__temperatures_df = iris_temperatures_df
            # save data to preprocessed data folder
            self.__temperatures_df.to_hdf(
                self.preprocessed_data, key="iris_temperatures_data"
            )
        return self.__temperatures_df
