"""
Module for data handling
"""

__version__ = "0.1.0"
__author__ = "Nils Artiges"
__licence__ = "Apache2.0"

import pandas as pd
import geopandas as gp
from glob import glob
import os

# module relative paths
proj_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


@pd.api.extensions.register_dataframe_accessor("split")
class Splitter:
    """Pandas accessor to split data in training and prediction (validation) datasets."""

    def __init__(self, pandas_obj: pd.DataFrame):
        self.__df = pandas_obj

    @property
    def training(self) -> pd.DataFrame:
        """Returns training data for this dataset (75% shuffled with fixed random seed)

        Returns:
            pd.DataFrame: 75% of dataframe shuffled with fixed random seed
        """
        training_df = self.__df.sample(frac=0.75, random_state=42)
        return training_df

    @property
    def prediction(self) -> pd.DataFrame:
        """Returns prediction / validation data

        Returns:
            pd.DataFrame: 25% of dataframe shuffled with fixed random seed, not in training set.
        """
        training_index = self.training.index
        test_df = self.__df[~self.__df.index.isin(training_index)]
        return test_df


class Data:
    """Class for raw data handling."""

    def __init__(self) -> None:
        """Instantiation of Data class."""
        self.data_raw_folder = os.path.join(proj_dir, "data", "raw")
        pass

    def enedis_df(self) -> pd.DataFrame:
        """Returns ENEDIS data from csv file.

        Returns:
            pd.DataFrame: ENEDIS thermo-energetic data (2017).
        """
        path = os.path.join(
            self.data_raw_folder,
            "consommation-electrique-par-secteur-dactivite-iris_2017.csv",
        )
        enedis_df = pd.read_csv(
            path,
            sep=";",
        )
        enedis_df["Code IRIS"] = pd.to_numeric(enedis_df["Code IRIS"], errors="coerce")
        enedis_df = enedis_df.dropna(subset=["Code IRIS"], axis=0)
        enedis_df["Code IRIS"] = enedis_df["Code IRIS"].astype(int)
        return enedis_df

    def insee_df(self) -> pd.DataFrame:
        """Returns INSEE dwelling statistics at iris scale from csv file.

        Returns:
            pd.DataFrame: INSEE dwellings statistics (2016).
        """
        path = os.path.join(
            self.data_raw_folder,
            "base-ic-logement-2016.xls",
        )
        insee_df = pd.read_excel(
            path,
            sheet_name="IRIS",
            header=5,
        )
        insee_df["IRIS"] = pd.to_numeric(insee_df["IRIS"], errors="coerce")
        insee_df = insee_df.dropna(subset=["IRIS"], axis=0)
        insee_df["IRIS"] = insee_df["IRIS"].astype(int)
        return insee_df

    def iris_borders_df(self) -> gp.GeoDataFrame:
        """Returns IRIS polygons as a geopandas dataframe from shapefile.

        Returns:
            gp.GeoDataFrame: IRIS polygons (2016) in Lambert93 projection.
        """
        path = os.path.join(
            self.data_raw_folder,
            "CONTOURS-IRIS_2-1__SHP__FRA_2017-06-30/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2016/CONTOURS-IRIS_2-1_SHP_LAMB93_FE-2016/CONTOURS-IRIS.shp",
        )
        iris_shp_df = gp.read_file(path)
        iris_shp_df["CODE_IRIS"] = pd.to_numeric(
            iris_shp_df["CODE_IRIS"], errors="coerce"
        )
        iris_shp_df = iris_shp_df.dropna(subset=["CODE_IRIS"], axis=0)
        iris_shp_df["CODE_IRIS"] = iris_shp_df["CODE_IRIS"].astype(int)
        return iris_shp_df

    def weather_df(self) -> pd.DataFrame:
        """Weather metrics of main French weather stations.

        Returns:
            pd.DataFrame: Synop French weather data for 2017.
        """
        df = pd.DataFrame()
        folder_path = os.path.join(
            self.data_raw_folder, "synop_weather_data", "synop.*.csv"
        )
        for path in glob(folder_path):
            df = df.append(pd.read_csv(path, sep=";", na_values=["mq"]))
        df["time"] = pd.to_datetime(df["date"], format="%Y%m%d%H%M%S")
        df.drop(columns=["date", "Unnamed: 59"], inplace=True)
        df.set_index("time", inplace=True, drop=True)
        return df

    def weather_stations_df(self) -> gp.GeoDataFrame:
        """List and localisation of main French weather stations used in Synop data.

        Returns:
            gp.GeoDataFrame: French weather stations names and coordinates.
        """
        df = pd.DataFrame()
        file_path = os.path.join(
            self.data_raw_folder, "synop_weather_data", "postesSynop.csv"
        )
        df = pd.read_csv(file_path, sep=";")
        gdf = gp.GeoDataFrame(df, geometry=gp.points_from_xy(df.Longitude, df.Latitude))
        gdf.set_index("ID", drop=True, inplace=True)
        gdf = gdf.set_crs("EPSG:4326")
        gdf = gdf.to_crs("EPSG:2154")
        return gdf

    def france_metro_df(self) -> gp.GeoDataFrame:
        """Geodataframe of metropolitan France regions (2018).

        Returns:
            gp.GeoDataFrame: Metropolitan France regions polygons (2018).
        """
        file_path = os.path.join(
            self.data_raw_folder, "france_shapefiles", "regions-20180101.shp"
        )
        fr = gp.read_file(file_path)
        fr.geometry = fr.geometry.simplify(0.01)
        fr_metro = fr[
            ~fr.nom.isin(
                ["La Réunion", "Corse", "Martinique", "Guadeloupe", "Guyane", "Mayotte"]
            )
        ]
        return fr_metro.to_crs("EPSG:2154")


class Weather:
    """Class for weather data handling."""

    def __init__(self, data: Data) -> None:
        """Instantiation of Weather class.

        Args:
            data (Data): Data object.
        """
        self.__data = data
        self.__weather_df = pd.DataFrame()
        self.__weather_stations_df = pd.DataFrame()
        self.__metro_stations_df = pd.DataFrame()
        pass

    @property
    def weather_df(self) -> pd.DataFrame:
        """Returns weather dataframe.

        Returns:
            pd.DataFrame: Weather data from main French stations in 2017.
        """
        if self.__weather_df.empty:
            df = self.__data.weather_df()
            self.__weather_df = df
        return self.__weather_df

    @property
    def stations_df(self) -> gp.GeoDataFrame:
        """Returns dataframe of French weather sations with localisation.

        Returns:
            gp.GeoDataFrame: French weather sations with localisation.
        """
        if self.__weather_stations_df.empty:
            df = self.__data.weather_stations_df()
            self.__weather_stations_df = df
        return self.__weather_stations_df

    @property
    def metro_stations_df(self) -> gp.GeoDataFrame:
        """Returns metropolitan French weather stations.

        Returns:
            gp.GeoDataFrame: Metropolitan French weather stations.
        """
        if self.__metro_stations_df.empty:
            stations_df = self.stations_df
            fr_metro = self.__data.france_metro_df()
            metro_stations_df = stations_df[
                stations_df.within(fr_metro.geometry.unary_union.convex_hull)
            ]
            self.__metro_stations_df = metro_stations_df
        return self.__metro_stations_df

    def plot_metro_stations(self):
        """Plots metropolitan French stations over a map.

        Returns:
            Matplotlib Axis: axis of figure.
        """
        stations_df = self.metro_stations_df
        ax = self.__data.france_metro_df().plot(figsize=(15, 15))
        stations_df.plot(ax=ax, color="red")
        return ax

    @property
    def temperatures_metro_celsius(self) -> gp.GeoDataFrame:
        """Returns celsius temperatures of metropolitan French weather stations (2017).

        Returns:
            gp.GeoDataFrame: Temperatures in celsius.
        """
        df = self.weather_df
        weather_metro = df[df.numer_sta.isin(self.metro_stations_df.index)].copy()
        weather_metro.loc[:, "t"] = weather_metro["t"] - 273.15
        weather_metro = weather_metro[["numer_sta", "t"]].join(
            self.metro_stations_df, on="numer_sta"
        )
        return gp.GeoDataFrame(weather_metro)
