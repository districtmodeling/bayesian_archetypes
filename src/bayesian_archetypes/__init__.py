# -*- coding: utf-8 -*-

__version__ = "0.1.0"
__author__ = "Nils Artiges"
__licence__ = "Apache2.0"

from pkg_resources import get_distribution, DistributionNotFound

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = "unknown"
finally:
    del get_distribution, DistributionNotFound
