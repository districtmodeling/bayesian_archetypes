"""
Module for data extraction and treatment from TABULA webtool
"""

__version__ = "0.1.0"
__author__ = "Nils Artiges"
__licence__ = "Apache2.0"

import requests as rq
import pandas as pd
import os

# module relative paths
proj_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


class TabulaAPI:
    """Class for handling of TABULA webtool API endpoints and extracted data."""

    def __init__(self) -> None:
        """Instantiation of TabulaAPI object."""
        self._country_endpoint = "https://webtool.building-typology.eu/data/matrix/building/{}/p/0/o/0/l/10/dc/1234567890123"
        self._building_endpoint = "https://webtool.building-typology.eu/data/adv/building/detail/{}/bv/{}/dc/1234567890123"
        self.__french_buildings_df = pd.DataFrame()
        self.preprocessed_data_dir = os.path.join(proj_dir, "data", "preprocessed")
        pass

    def get_buildings_by_country(self, country_tag: str) -> dict:
        """Returns TABULA average buildings per country.

        Args:
            country_tag (str): Country tag, such as 'fr' for France.

        Returns:
            dict: buildings for selected country.
        """
        resp = rq.get(self._country_endpoint.format(country_tag))
        resp.raise_for_status()
        return resp.json()["data"]

    def get_building_by_id(self, building_id: str, refurbishment_id: int) -> dict:
        """Returns building data from its specific id.

        Args:
            building_id (str): Building ID as used in Tabula webtool.
            refurbishment_id (int): in [1,2,3] (1=actual state, 2=usual refurbishment, 3=advanced refurbishment).

        Returns:
            dict: selected building data.
        """
        # refurbishment_id is in [1,2,3] (1=actual state)
        resp = rq.get(self._building_endpoint.format(building_id, refurbishment_id))
        resp.raise_for_status()
        return resp.json()["data"]

    def get_building_ids_by_country(self, country_tag: str) -> dict:
        """Returns building IDs per country.

        Args:
            country_tag (str): country tag, such as 'fr' for France.

        Returns:
            dict: Building IDs for selected country.
        """
        items = self.get_buildings_by_country(country_tag)
        bldgs_ids = []
        for i in items:
            for c in range(1, 5):
                bldgs_ids.append(
                    i["code_buildingtype_column{}".format(c)]
                    + "."
                    + i["suffix_building_column{}".format(c)]
                )
        return bldgs_ids

    def get_main_building_params(
        self, building_id: str, refurbishment_id: int
    ) -> pd.DataFrame:
        """Returns building parameters as a dataframe

        Args:
            building_id (str): Building ID as in Tabula Webtool.
            refurbishment_id (int): Refurbishment id of building (1, 2 or 3).

        Returns:
            pd.DataFrame:
            "year1_building" : construction year of the building (or first year of the construction period)
            "year2_building" : construction year of the building (or last year of the construction period)
            "code_buildingsizeclass" : building typology (single family house, multi terraced house ...)
            "a_c_ref" : energy reference area (conditioned floor area, internal dimensions)
            "n_apartment" : number of apartments
            "heatingdays" : number of days per year during heating season (average daily temperature is below or equal the base temperature)
            "theta_e" : average external air temperature during the heating season
            "theta_i": internal temperature
            "sum_deltat_for_heatingdays" : accumulated difference between internal and external temperature
            "q_h_nd" : energy need for heating per area for one building
        """
        var_list = [
            "year1_building",
            "year2_building",
            "code_buildingsizeclass",
            "a_c_ref",
            "n_apartment",
            "heatingdays",
            "theta_e",
            "theta_i",
            "sum_deltat_for_heatingdays",
            "q_h_nd",
        ]
        b_data = self.get_building_by_id(
            building_id=building_id, refurbishment_id=refurbishment_id
        )
        df = pd.DataFrame(data=b_data)
        df["index"] = 0
        df = df.pivot(values="val", columns="var", index="index")
        df.columns.name = None
        df = df[var_list]
        df["building_id"] = building_id
        df["refurbishment_id"] = refurbishment_id
        # compute HLC values in kwh/K-1
        df["hlc_kwh_k"] = (
            df["a_c_ref"]
            * df["q_h_nd"]
            / (df["sum_deltat_for_heatingdays"] * df["n_apartment"])
        )
        return df

    def get_buildings_df_by_country(self, country_tag: str) -> pd.DataFrame:
        """Returns buildings data as dataframe for a given country code.

        Args:
            country_tag (str): Country tag, such as 'fr' for France.

        Returns:
            pd.DataFrame: Buildings dataframe for selected country.
        """
        building_ids = self.get_building_ids_by_country(country_tag=country_tag)
        df = pd.DataFrame()
        for b_id in building_ids:
            for r_id in [1, 2, 3]:
                df = df.append(
                    self.get_main_building_params(
                        building_id=b_id, refurbishment_id=r_id
                    )
                )
        str_cols = ["code_buildingsizeclass", "building_id"]
        num_cols = [c for c in df.columns if c not in str_cols]
        df[str_cols] = df[str_cols].astype(str)
        df[num_cols] = df[num_cols].astype(float)
        df["refurbishment_id"] = df["refurbishment_id"].astype(int)
        return df

    @property
    def french_buildings_df(self) -> pd.DataFrame:
        """Returns buildings data from TABULA webtool for France as a dataframe.

        Returns:
           pd.DataFrame: French Average buildings data from TABULA webtool.
        """
        file_path = os.path.join(
            self.preprocessed_data_dir, "tabula_french_buildings.csv"
        )
        if os.path.isfile(file_path) and self.__french_buildings_df.empty:
            self.__french_buildings_df = pd.read_csv(file_path, index_col=0)
        if self.__french_buildings_df.empty:
            df = self.get_buildings_df_by_country("fr")
            self.__french_buildings_df = df
            self.__french_buildings_df.to_csv(file_path)
        return self.__french_buildings_df
