# Changelog

## Version 0.1.1
- Graphs improved for publication.
## Version 0.1.0

- Original publication of data, code and results at submission stage for paper "Bayesian inference of dwellings energy signature at national scale: case of the French residential stock".
