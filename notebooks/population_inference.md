---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: 'Python 3.9.2 64-bit (''bayesian'': conda)'
    language: python
    name: python392jvsc74a57bd0d5a773d2c9fec8ba7eac49e9c5cc5da4f63beb0ed8f6b48706eb374fd6e587d2
---

# Experiments for estimation of joint probabilities of dwelling sub-categories (poplulation inference)

---
**WARNING**

This model is provided for experimentations on joint distribution (population) inference. Results here are not consolidated nor validated.
---

```python
## imports
# local package
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp
# science packs
import pymc3 as pm
import numpy as np
import pandas as pd
import theano.tensor as tt
# graphing packs
import arviz as az
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
# utilities
import os, sys
import logging as log
from tqdm import tqdm
from pathlib import Path
# figure parameters
FIGSIZE = (12, 6)
rcParams['figure.figsize'] = FIGSIZE
rcParams["savefig.dpi"] = 250
rcParams["savefig.format"] = "png"
# callbacks
class Progress(tqdm):
    def __init__(self):
        super().__init__()
        self.mod = 100
        self.counter = 0
    def do_update(self, trace, draw):
        if self.total == None:
            self.total = trace.draws
            self.current_ndraws = 0
        if (draw.chain == 0 and self.counter < trace.draw_idx // self.mod) or trace.draw_idx == self.total:
            self.update(trace.draw_idx - self.current_ndraws)
            self.counter += 1
            self.current_ndraws = trace.draw_idx
```

```python
# Select if reusing previous inference data or rerun inference
# select model name and type (testing or not)
# (this is a default parametrized cell | used values are copied by papermill below)
run_inference = True
model_id = "population"
nb_dir = os.path.dirname(os.path.realpath("__file__"))
```

```python
# file paths
proj_dir = Path(nb_dir).parents[0]
data_dir = proj_dir / "data" / "inference" / "local"
pictures_dir = proj_dir / "notebooks" / "pictures"
tables_dir  = proj_dir / "notebooks" / "tables"
print(proj_dir)
```

```python
### setup data
data = io.Data()
iris = cp.Iris(data)

## global values
hlc_avg_mean = iris.thermo_df["hlc_avg"].mean()
hlc_avg_std = iris.thermo_df["hlc_avg"].std()
n_iris = len(iris.thermo_df)
batch_size = n_iris

ratios_dates = iris.date_ratios.values
ratios_surf = iris.surface_ratios.values
```

In the following inference model, we try to infer conditional probabilities of ratios from the following formulas:

$$
\boldsymbol{p\left(\textit{surf}\right)}=\boldsymbol{p\left(\textit{type_date}\right)}\cdot A\\
\boldsymbol{p\left(\textit{type_date}\right)}=\boldsymbol{p\left(\textit{surf}\right)}\cdot B
$$

where the matrix $A$ holds conditional probabilities $p\left(\textit{surf}_{i}|\textit{type_date}_{j}\right)$ as its coefficients, and $B$ for $p\left(\textit{type_date}_{i}|\textit{surf}_{j}\right)$.

```python
with pm.Model() as model:
    r_dates = pm.Data("ratios_dates", ratios_dates[:batch_size, :])
    r_surf = pm.Data("ratios_surf", ratios_surf[:batch_size, :])
    p_s_d = pm.Uniform("p_s_d", shape=(14, 7)) # dates * surf
    p_d_s = pm.Uniform("p_d_s", shape=(7, 14)) # surf * dates
    std_surf = pm.HalfNormal("std_surf", sigma=0.001, shape=(7))
    std_dates = pm.HalfNormal("std_dates", sigma=0.001, shape=(14))
    est_r_dates = r_surf.dot(p_d_s)
    est_r_surf = r_dates.dot(p_s_d)
    pm.Normal("lkh_dates", mu=est_r_dates, sigma=std_dates, observed=r_dates, shape=(batch_size, 14))
    pm.Normal("lkh_surf", mu=est_r_surf, sigma=std_surf, observed=r_surf, shape=(batch_size, 7))
    
with model:
    prior = pm.sample_prior_predictive(samples=100)
```

```python
if run_inference:
    with model:
        idata = pm.sample(return_inferencedata=True, init="advi", tune=200, draws=500, chains=4)
```

```python
saved_data_file = data_dir / "{}_data.nc".format(model_id)
if run_inference:
    az.to_netcdf(idata, saved_data_file)
else:
    idata = az.from_netcdf(saved_data_file)
```

```python
az.plot_trace(idata, compact=True)
```

```python
## Sampling stats summary
stats_table = az.summary(idata, kind="diagnostics").describe().drop(axis=0, labels=["count", "25%", "50%", "75%"])
stats_table
```

The sampler converged properly without any divergences. However, some parameters are clearly under sampled and not reliable.

```python
summary = az.summary(idata)
p_s_d = np.reshape(summary.loc[summary.index.str.startswith('p_s_d')]["mean"].values, (14, 7))
p_d_s = np.reshape(summary.loc[summary.index.str.startswith('p_d_s')]["mean"].values, (7, 14))
```

we can plot $A$ and $B$ matrices to see repartition of conditional probabilities.  
Many values are very close to 0. This mean than some categories are almost inexistant in the whole population.

```python
sns.heatmap(p_s_d, cmap="viridis")
```

```python
sns.heatmap(p_d_s, cmap="viridis")
```

We can also plot the estimation of ratios error using our conditional probabilities model.  
Errors can go up to $20\%$ which is not neglectable.

```python
# error on estimates
sns.displot((ratios_dates - ratios_surf.dot(p_d_s)))
```

```python
sns.displot((ratios_surf - ratios_dates.dot(p_s_d)))
```

We can use these conditional probabilities to estimate joint ratios with the following formula:

$$2.ratios\_joint_{i,j}	\approx p\left(\textit{surf}_{i}|\textit{type_date}_{j}\right).p\left(\textit{type_date}_{j}\right) + p\left(\textit{type_date}_{j}|\textit{surf}_{i}\right).p\left(\textit{surf}_{i}\right)$$

```python
joint_ratios = 0.5*(np.reshape(p_s_d, 98) * np.tile(ratios_dates, 7)) + 0.5*(np.reshape(p_d_s, 98) * np.tile(ratios_surf, 14))
```

```python
# build labels
surf = [col.split("ratio_")[1] for col in iris.thermo_df.columns if "surf_" in col]
date = [col.split("ratio_")[1] for col in iris.thermo_df.columns if "date_" in col]
labels = []
for s in surf:
    for d in date:
        labels.append("ratio_{}_{}".format(d, s))
```

After filtering of under represented ratios and normalization (sum of ratios must be equal to to one), we can save them for further experimentations.

```python
joint_ratios_df = pd.DataFrame(columns=labels, data=joint_ratios)
valid_categories = joint_ratios_df.describe().loc["mean"][~(joint_ratios_df.describe().loc["mean"] < 0.005)].index
joint_ratios_df = joint_ratios_df[valid_categories]
# 50 joint categories are represented
len(joint_ratios_df.columns)
```

```python
joint_ratios_df.head()
```

```python
# renormalization
joint_ratios_df = joint_ratios_df.div(joint_ratios_df.sum(axis=1), axis=0)
```

```python
joint_ratios_df.head()
```

```python
joint_ratios_df.to_csv(proj_dir / "data" / "preprocessed" / "joint_ratios.csv")
```
