---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: 'Python 3.9.2 64-bit (''bayesian'': conda)'
    language: python
    name: python392jvsc74a57bd0d5a773d2c9fec8ba7eac49e9c5cc5da4f63beb0ed8f6b48706eb374fd6e587d2
---

<!-- #region papermill={"duration": 0.032107, "end_time": "2021-07-27T14:28:44.460141", "exception": false, "start_time": "2021-07-27T14:28:44.428034", "status": "completed"} tags=[] -->
# Inference of energy signature of houses and buildings from aggregated national data: Experientation with mixture model

---
**WARNING**
This model is provided for fit experimentations with mixture models. In this case, infered parameters are biased since we cannot build a correct physical interpretation for this model.
---
<!-- #endregion -->

```python papermill={"duration": 4.34159, "end_time": "2021-07-27T14:28:48.824157", "exception": false, "start_time": "2021-07-27T14:28:44.482567", "status": "completed"} tags=[]
## imports
# local package
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp
# science packs
import pymc3 as pm
import numpy as np
import pandas as pd
import theano.tensor as tt
# graphing packs
import arviz as az
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
# utilities
import os, sys
import logging as log
from tqdm import tqdm
from pathlib import Path
tp = tt.printing.Print
# figure parameters
FIGSIZE = (12, 6)
rcParams['figure.figsize'] = FIGSIZE
rcParams["savefig.dpi"] = 250
rcParams["savefig.format"] = "png"
# callbacks
class Progress(tqdm):
    def __init__(self):
        super().__init__()
        self.clear()
        self.mod = 100
        self.counter = 0
    def do_update(self, trace, draw):
        if self.total == None:
            self# Inference of energy signature of houses and buildings from aggregated national data: Experientation with mixture model
            self.current_ndraws = 0
        if draw.chain == 0 and self.counter < trace.draw_idx // self.mod:
            self.update(trace.draw_idx - self.current_ndraws)
            self.counter += 1
            self.current_ndraws = trace.draw_idx
        if draw.chain == 0 and trace.draw_idx == self.total:
            self.update(trace.draw_idx - self.current_ndraws)
            self.close()
            
```

```python papermill={"duration": 0.049997, "end_time": "2021-07-27T14:28:48.898397", "exception": false, "start_time": "2021-07-27T14:28:48.848400", "status": "completed"} tags=["injected-parameters"]
# Parameters
model_id = "types"
run_inference = True
nb_dir = "/home/nils/Documents/ProjetsDev/bayesian_archetypes/notebooks"

```

```python papermill={"duration": 0.038015, "end_time": "2021-07-27T14:28:48.982354", "exception": false, "start_time": "2021-07-27T14:28:48.944339", "status": "completed"} tags=[]
# file paths
proj_dir = Path(nb_dir).parents[0]
data_dir = proj_dir / "data" / "inference" / "local"
pictures_dir = proj_dir / "notebooks" / "pictures"
tables_dir  = proj_dir / "notebooks" / "tables"
print(proj_dir)
```

<!-- #region papermill={"duration": 0.05559, "end_time": "2021-07-27T14:28:49.065652", "exception": false, "start_time": "2021-07-27T14:28:49.010062", "status": "completed"} tags=[] -->
## Data setup

First, we setup the data required for our selected model.
The data is preprocessed from raw data sources in tabular form where each row matches one IRIS: specific geographic area of metropolitan France of roughly 500 to 2000 dwellings:

- ratios for dwellings categories
- number of dwellings per IRIS
- yearly aggregated energy signature parameters:
    - $hlc_{avg}$, the average heat loss coefficient per IRIS
    - ${base\ consumption}_{avg}$, the average consumption not due to temperature variations
    - $T_s$, the temperature switch point

Data is split in training and validation sets (respectively 75% and 25% of shuffled original data).
<!-- #endregion -->

```python papermill={"duration": 0.133747, "end_time": "2021-07-27T14:28:49.228397", "exception": false, "start_time": "2021-07-27T14:28:49.094650", "status": "completed"} tags=[]
### setup data
data = io.Data()
iris = cp.Iris(data)

if model_id == "types":
    ratios_df = iris.type_ratios
elif model_id == "surfaces":
    ratios_df = iris.surface_ratios
elif model_id == "dates":
    ratios_df = iris.date_ratios
elif model_id == "joint":
    ratios_df = iris.joint_ratios
    
model_id = "mixture_" +  model_id

rename_dict = {c: c.split("ratio_")[1] for c in ratios_df.columns}    
ratios_df = ratios_df.rename(columns=rename_dict)

labels = ratios_df.columns
n_sites_label = "dwellings_number"
```

<!-- #region papermill={"duration": 0.023888, "end_time": "2021-07-27T14:28:49.276073", "exception": false, "start_time": "2021-07-27T14:28:49.252185", "status": "completed"} tags=[] -->
## Model specification

Then, we specify the Model class:
<!-- #endregion -->

```python papermill={"duration": 0.049404, "end_time": "2021-07-27T14:28:49.349549", "exception": false, "start_time": "2021-07-27T14:28:49.300145", "status": "completed"} tags=[]
# model class
class IrisModel(pm.Model):
    def __init__(self, name="", model=None) -> pm.Model:
        super().__init__(name=name, model=model)
        self.thermo_df = iris.thermo_df
        self.ratios_df = ratios_df
        self.input_vars = []
        self.output_vars = []
    
    def setup(self, input_vars: list, output_vars: list):
        # define datasets
        self.input_vars = input_vars
        self.output_vars = output_vars
        self.set_data_train()
        n_x = len(self.input_vars)
        n_y = len(self.output_vars)
        # define priors
        BNormal = pm.Bound(pm.Normal, lower=1E-3, upper=100)
        mu = BNormal("mu", mu=0.75, sigma=20, dims=("variables", "categories"))
        sigma = BNormal("sigma", mu=0, sigma=20, dims=("variables", "categories"))
        #epsilon = pm.HalfNormal("epsilon", sigma=20, dims="variables")
        #std_err = epsilon / tt.sqrt(self.n_sites[:, np.newaxis])
        # define model
        mu = tt.tile(mu.dimshuffle(("x", 0, 1)), (self.n_iris,1,1))
        sigma = tt.tile(sigma.dimshuffle(("x", 0, 1)), (self.n_iris,1,1))
        tp("mu")(mu.shape)
        tp("sigma")(sigma.shape)
        y_cat = pm.Gamma.dist(mu=mu, sigma=sigma, shape=(self.n_iris, self.n_var, self.n_cat))
        w = tt.tile(self.ratios.dimshuffle((0, "x", 1)), (1,self.n_var,1))
        tp("t")
        tp("w")(w.shape)
        tp("y")(self.y_data.shape)
        # define likelihood
        lkh = pm.Mixture("iris_obs", w=w, comp_dists=y_cat, observed=self.y_data, dims=("iris", "variables"))
        pass

    def set_data_train(self):
        # setup data containers for training phase
        self.coords = {
            "iris": self.thermo_df.split.training.index,
            "categories": self.input_vars,
            "variables": self.output_vars
        }
        self.n_iris = len(self.coords["iris"])
        self.n_cat = len(self.coords["categories"])
        self.n_var = len(self.coords["variables"])
        x = self.ratios_df.split.training[self.input_vars].values
        y = self.thermo_df.split.training[self.output_vars].values
        self.n_iris = len(self.thermo_df.split.training)
        n_sites = self.thermo_df.split.training[n_sites_label].values
        ratios = pm.Data("ratios", x, dims=("iris", "categories"))
        y_data = pm.Data("y_data", y, dims=("iris", "variables"))
        #n_sites = pm.Data("n_sites", n_sites, dims="iris")
        pass
    
    def set_data_prediction(self):
        # update data containers for prediction/validation phase
        self.coords["iris"] = self.thermo_df.split.prediction.index
        x = self.ratios_df.split.prediction[self.input_vars].values
        y = self.thermo_df.split.prediction[self.output_vars].values
        self.n_iris = len(self.thermo_df.split.prediction)
        n_sites = self.thermo_df.split.prediction[n_sites_label].values
        ratios = pm.set_data({"ratios": x})
        y_data = pm.set_data({"y_data": y})
        #n_sites = pm.set_data({"n_sites": n_sites})
        pass
```

```python papermill={"duration": 8.749619, "end_time": "2021-07-27T14:28:58.124767", "exception": false, "start_time": "2021-07-27T14:28:49.375148", "status": "completed"} tags=[]
with IrisModel() as model:
    model.setup(input_vars=labels, output_vars=["hlc_avg", "base_consumption_avg", "Ts"])
```

```python papermill={"duration": 0.970244, "end_time": "2021-07-27T14:28:59.119870", "exception": false, "start_time": "2021-07-27T14:28:58.149626", "status": "completed"} tags=[]
graph = pm.model_to_graphviz(model)
graph.format = "png"
graph.render(pictures_dir / "{}_graph".format(model_id))
graph
```

<!-- #region papermill={"duration": 0.026929, "end_time": "2021-07-27T14:28:59.173323", "exception": false, "start_time": "2021-07-27T14:28:59.146394", "status": "completed"} tags=[] -->
## Inference of model parameters

We solve the inference problem with NUTS algorithm after an initialization using ADVI.
<!-- #endregion -->

```python papermill={"duration": 1612.966962, "end_time": "2021-07-27T14:55:52.165361", "exception": false, "start_time": "2021-07-27T14:28:59.198399", "status": "completed"} tags=[]
if run_inference:
    with model:
        pbar = Progress() # progress bar for logging with papermill
        trace = pm.sample(target_accept=0.9, return_inferencedata=False, init="advi", chains=4, draws=800, tune=200, cores=4,
                         callback=pbar.do_update)
        idata = az.from_pymc3(trace, model=model)
```

```python papermill={"duration": 54.13594, "end_time": "2021-07-27T14:56:46.341208", "exception": false, "start_time": "2021-07-27T14:55:52.205268", "status": "completed"} tags=[]
saved_data_file = data_dir / "{}_data.nc".format(model_id)
if run_inference:
    az.to_netcdf(idata, saved_data_file)
else:
    idata = az.from_netcdf(saved_data_file)
```

<!-- #region papermill={"duration": 0.040984, "end_time": "2021-07-27T14:56:46.422553", "exception": false, "start_time": "2021-07-27T14:56:46.381569", "status": "completed"} tags=[] -->
## Convergence assessment

To check the convergence quality of the inference process, we report the following statistics:

- $MCSE$: Monte Carlo standard error. Must be as low as possible.
- $ESS$: Effective Sample size. Low values for some parameter indicate poor sampling performance (higher autocorrelation between samples).
- $\hat{R}$: Indicator of similarity between Monte Carlo sampling chains. Must be comprised between 1 and 1.1
- NUTS sampling above should have (or only very few) divergent transitions.
<!-- #endregion -->

```python papermill={"duration": 0.18724, "end_time": "2021-07-27T14:56:46.652869", "exception": false, "start_time": "2021-07-27T14:56:46.465629", "status": "completed"} tags=[]
az.summary(idata, kind="diagnostics").transpose()
```

```python papermill={"duration": 0.188334, "end_time": "2021-07-27T14:56:46.880896", "exception": false, "start_time": "2021-07-27T14:56:46.692562", "status": "completed"} tags=[]
## Sampling stats summary
stats_table = az.summary(idata, kind="diagnostics").describe().drop(axis=0, labels=["count", "25%", "50%", "75%"])
stats_table.to_latex(tables_dir / "{}_stats_table.tex".format(model_id), float_format='%.3g')
stats_table
```

```python papermill={"duration": 0.191503, "end_time": "2021-07-27T14:56:47.117813", "exception": false, "start_time": "2021-07-27T14:56:46.926310", "status": "completed"} tags=[]
# mu effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["sigma"]).loc["ess_tail",:].sort_values("mu").set_index("categories").rename(columns={"mu": "mu: effective sample size"})
```

```python papermill={"duration": 0.159985, "end_time": "2021-07-27T14:56:47.319444", "exception": false, "start_time": "2021-07-27T14:56:47.159459", "status": "completed"} tags=[]
# sigma effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["mu"]).loc["ess_tail",:].sort_values("sigma").set_index("categories").rename(columns={"sigma": "sigma: effective sample size"})
```

```python papermill={"duration": 0.184728, "end_time": "2021-07-27T14:56:47.548984", "exception": false, "start_time": "2021-07-27T14:56:47.364256", "status": "completed"} tags=[]
az.summary(idata, fmt="long").transpose()
```

<!-- #region papermill={"duration": 0.043843, "end_time": "2021-07-27T14:56:47.640731", "exception": false, "start_time": "2021-07-27T14:56:47.596888", "status": "completed"} tags=[] -->
We can visually evaluate the quality of sampling by examining its regularity in the trace plot below.
<!-- #endregion -->

```python papermill={"duration": 2.573046, "end_time": "2021-07-27T14:56:50.255877", "exception": false, "start_time": "2021-07-27T14:56:47.682831", "status": "completed"} tags=[]
az.plot_trace(idata, compact=True, divergences=None)
plt.savefig(pictures_dir / "{}_trace".format(model_id))
```

<!-- #region papermill={"duration": 0.048344, "end_time": "2021-07-27T14:56:50.354753", "exception": false, "start_time": "2021-07-27T14:56:50.306409", "status": "completed"} tags=[] -->
Besides, posterior predictive sampling of output variables on training and validation datasets must match as much as possible observed distributions.
<!-- #endregion -->

```python papermill={"duration": 329.054608, "end_time": "2021-07-27T15:02:19.456783", "exception": false, "start_time": "2021-07-27T14:56:50.402175", "status": "completed"} tags=[]
# sample posterior predictive
if run_inference:
    with model:
        ppc_fit = pm.sample_posterior_predictive(trace, samples=100)
        idata_post_pred = az.from_pymc3(trace=trace, posterior_predictive=ppc_fit)
    az.to_netcdf(idata_post_pred, data_dir / "{}_idata_post_pred.nc".format(model_id))
else:
    idata_post_pred = az.from_netcdf(data_dir / "{}_idata_post_pred.nc".format(model_id))
```

```python papermill={"duration": 0.123384, "end_time": "2021-07-27T15:02:19.630381", "exception": true, "start_time": "2021-07-27T15:02:19.506997", "status": "failed"} tags=[]
pc_plot = az.plot_ppc(idata_post_pred, figsize=(12,4), coords={"variables": ["hlc_avg","base_consumption_avg", "Ts"]}, flatten=["iris"], mean=False)
pc_plot[0].set_xlim([0,5])
pc_plot[1].set_xlim([0,10])
plt.suptitle("Posterior predictive distributions for training data", fontsize=16)
plt.savefig(pictures_dir / "{}_ppc_training".format(model_id))
```

```python papermill={"duration": null, "end_time": null, "exception": null, "start_time": null, "status": "pending"} tags=[]
# sample posterior predictive on validation dataset
if run_inference:
    with model:
        model.set_data_prediction()
        ppc_valid = pm.sample_posterior_predictive(trace, samples=100)
        idata_post_valid = az.from_pymc3(trace=trace, posterior_predictive=ppc_valid)
    az.to_netcdf(idata_post_valid, data_dir / "{}_idata_post_valid.nc".format(model_id))
else:
    idata_post_valid = az.from_netcdf(data_dir / "{}_idata_post_valid.nc".format(model_id))
```

```python papermill={"duration": null, "end_time": null, "exception": null, "start_time": null, "status": "pending"} tags=[]
ppc_plot = az.plot_ppc(idata_post_valid, figsize=(12,4), coords={"variables": ["hlc_avg","base_consumption_avg", "Ts"]}, flatten=["iris"], mean=False)
ppc_plot[0].set_xlim([0,5])
ppc_plot[1].set_xlim([0,10])
plt.suptitle("Posterior predictive distributions for validation data", fontsize=16)
plt.savefig(pictures_dir / "{}_ppc_validation".format(model_id))
```

<!-- #region papermill={"duration": null, "end_time": null, "exception": null, "start_time": null, "status": "pending"} tags=[] -->
## Results interpretation

To interpret inferred parameters, we can compare them on a tree plot with teir confidence intervals.
<!-- #endregion -->

```python papermill={"duration": null, "end_time": null, "exception": null, "start_time": null, "status": "pending"} tags=[]
variables = ["hlc_avg", "base_consumption_avg", "Ts"]
x_labels = ["dwelling HLC (kWh/K)", "dwelling base consumption (MWh)", "dwelling Ts (°C)"]
var_colors = ["blue", "red",  "teal"]
params = ["mu", "sigma"]
fig, ax = plt.subplots(len(params), len(variables), gridspec_kw={'height_ratios': [1, 1]})
fig.set_figheight(max(5, len(labels)*10/12))
for i, p in enumerate(params):
    for j, v in enumerate(variables):
        axis = ax[i][j]
        az.plot_forest(idata, kind="forestplot", coords={"variables":v}, var_names=p, combined=True, ax=axis, colors=var_colors[j])
        if i > 0:
            axis.set_title("")
        if i == len(params) - 1:
            axis.set_xlabel(x_labels[j], fontsize="x-large")
        if j > 0:
            axis.get_yaxis().set_visible(False)
plt.tight_layout()
plt.savefig(pictures_dir / "{}_treeplot".format(model_id))
```

```python

```
