---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: 'Python 3.9.2 64-bit (''bayesian'': conda)'
    language: python
    name: python392jvsc74a57bd0d5a773d2c9fec8ba7eac49e9c5cc5da4f63beb0ed8f6b48706eb374fd6e587d2
---

<!-- #region papermill={"duration": 0.025988, "end_time": "2021-07-29T18:32:35.685709", "exception": false, "start_time": "2021-07-29T18:32:35.659721", "status": "completed"} tags=[] -->
# Inference of energy signature of houses and buildings from aggregated national data
<!-- #endregion -->

```python papermill={"duration": 4.055181, "end_time": "2021-07-29T18:32:39.765832", "exception": false, "start_time": "2021-07-29T18:32:35.710651", "status": "completed"} tags=[]
## imports
# local package
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp
# science packs
import pymc3 as pm
import numpy as np
import pandas as pd
import theano.tensor as tt
# graphing packs
import arviz as az
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
# utilities
import os, sys
import logging as log
from tqdm import tqdm
from pathlib import Path
# figure parameters
FIGSIZE = (12, 6)
rcParams['figure.figsize'] = FIGSIZE
rcParams["savefig.dpi"] = 250
rcParams["savefig.format"] = "png"
# callbacks
class Progress(tqdm):
    def __init__(self):
        super().__init__()
        self.clear()
        self.mod = 100
        self.counter = 0
    def do_update(self, trace, draw):
        if self.total == None:
            self.total = trace.draws
            self.current_ndraws = 0
        if draw.chain == 0 and self.counter < trace.draw_idx // self.mod:
            self.update(trace.draw_idx - self.current_ndraws)
            self.counter += 1
            self.current_ndraws = trace.draw_idx
        if draw.chain == 0 and trace.draw_idx == self.total:
            self.update(trace.draw_idx - self.current_ndraws)
            self.close()
            
```

```python papermill={"duration": 0.035921, "end_time": "2021-07-29T18:32:39.828069", "exception": false, "start_time": "2021-07-29T18:32:39.792148", "status": "completed"} tags=["parameters"]
# Select if reusing previous inference data or rerun inference
# select model name and type (testing or not)
# (this is a default parametrized cell | used values are copied by papermill below)
run_inference = False
model_id = "types"
nb_dir = os.path.dirname(os.path.realpath("__file__"))
```

```python papermill={"duration": 0.03374, "end_time": "2021-07-29T18:32:39.888971", "exception": false, "start_time": "2021-07-29T18:32:39.855231", "status": "completed"} tags=["injected-parameters"]
# Parameters
model_id = "types"
run_inference = True
nb_dir = "/home/nils/Documents/ProjetsDev/bayesian_archetypes/notebooks"

```

```python papermill={"duration": 0.034103, "end_time": "2021-07-29T18:32:39.948715", "exception": false, "start_time": "2021-07-29T18:32:39.914612", "status": "completed"} tags=[]
# file paths
proj_dir = Path(nb_dir).parents[0]
data_dir = proj_dir / "data" / "inference" / "local"
pictures_dir = proj_dir / "notebooks" / "pictures"
tables_dir  = proj_dir / "notebooks" / "tables"
print(proj_dir)
```

<!-- #region papermill={"duration": 0.026641, "end_time": "2021-07-29T18:32:40.002027", "exception": false, "start_time": "2021-07-29T18:32:39.975386", "status": "completed"} tags=[] -->
## Data setup

First, we setup the data required for our selected model.
The data is preprocessed from raw data sources in tabular form where each row matches one IRIS: specific geographic area of metropolitan France of roughly 500 to 2000 dwellings:

- ratios for dwellings categories
- number of dwellings per IRIS
- yearly aggregated energy signature parameters:
    - $hlc_{avg}$, the average heat loss coefficient per IRIS
    - ${base\ consumption}_{avg}$, the average consumption not due to temperature variations
    - $T_s$, the temperature switch point

Data is split in training and validation sets (respectively 75% and 25% of shuffled original data).
<!-- #endregion -->

```python papermill={"duration": 0.14049, "end_time": "2021-07-29T18:32:40.168862", "exception": false, "start_time": "2021-07-29T18:32:40.028372", "status": "completed"} tags=[]
### setup data
data = io.Data()
iris = cp.Iris(data)

if model_id == "types":
    ratios_df = iris.type_ratios
elif model_id == "surfaces":
    ratios_df = iris.surface_ratios
elif model_id == "dates":
    ratios_df = iris.date_ratios
elif model_id == "joint":
    ratios_df = iris.joint_ratios

rename_dict = {c: c.split("ratio_")[1] for c in ratios_df.columns}    
ratios_df = ratios_df.rename(columns=rename_dict)

labels = ratios_df.columns
n_sites_label = "dwellings_number"
```

<!-- #region papermill={"duration": 0.026655, "end_time": "2021-07-29T18:32:40.222799", "exception": false, "start_time": "2021-07-29T18:32:40.196144", "status": "completed"} tags=[] -->
## Model specification

Then, we specify the Model class:
<!-- #endregion -->

```python papermill={"duration": 0.051215, "end_time": "2021-07-29T18:32:40.300115", "exception": false, "start_time": "2021-07-29T18:32:40.248900", "status": "completed"} tags=[]
# model class
class IrisModel(pm.Model):
    def __init__(self, name="", model=None) -> pm.Model:
        super().__init__(name=name, model=model)
        self.thermo_df = iris.thermo_df
        self.ratios_df = ratios_df
        self.input_vars = []
        self.output_vars = []
    
    def setup(self, input_vars: list, output_vars: list):
        # define datasets
        self.input_vars = input_vars
        self.output_vars = output_vars
        self.set_data_train()
        n_x = len(self.input_vars)
        n_y = len(self.output_vars)
        # define priors
        BNormal = pm.Bound(pm.Normal, lower=1E-3, upper=100)
        mu = BNormal("mu", mu=0.75, sigma=20, dims=("categories", "variables"))
        sigma = BNormal("sigma", mu=0, sigma=20, dims=("categories", "variables"))
        #epsilon = pm.HalfNormal("epsilon", sigma=20, dims="variables")
        # define model
        mu_iris = self.ratios.dot(mu)
        var_iris = self.ratios.dot(sigma ** 2) 
        std_iris = tt.sqrt(var_iris)
        #std_err = epsilon / tt.sqrt(self.n_sites[:, np.newaxis])
        # define likelihood
        lkh = pm.Gamma("iris_obs", mu=mu_iris, sigma=std_iris, observed=self.y_data, dims=("iris","variables"))
        pass

    def set_data_train(self):
        # setup data containers for training phase
        self.coords = {
            "iris": self.thermo_df.split.training.index,
            "categories": self.input_vars,
            "variables": self.output_vars
        }
        x = self.ratios_df.split.training[self.input_vars].values
        y = self.thermo_df.split.training[self.output_vars].values
        self.n_iris = len(self.thermo_df.split.training)
        n_sites = self.thermo_df.split.training[n_sites_label].values
        ratios = pm.Data("ratios", x, dims=("iris", "categories"))
        y_data = pm.Data("y_data", y, dims=("iris", "variables"))
        #n_sites = pm.Data("n_sites", n_sites, dims="iris")
        pass
    
    def set_data_prediction(self):
        # update data containers for prediction/validation phase
        self.coords["iris"] = self.thermo_df.split.prediction.index
        x = self.ratios_df.split.prediction[self.input_vars].values
        y = self.thermo_df.split.prediction[self.output_vars].values
        self.n_iris = len(self.thermo_df.split.prediction)
        n_sites = self.thermo_df.split.prediction[n_sites_label].values
        ratios = pm.set_data({"ratios": x})
        y_data = pm.set_data({"y_data": y})
        #n_sites = pm.set_data({"n_sites": n_sites})
        pass
```

```python papermill={"duration": 4.53916, "end_time": "2021-07-29T18:32:44.865926", "exception": false, "start_time": "2021-07-29T18:32:40.326766", "status": "completed"} tags=[]
with IrisModel() as model:
    model.setup(input_vars=labels, output_vars=["hlc_avg", "base_consumption_avg", "Ts"])
```

```python papermill={"duration": 1.007955, "end_time": "2021-07-29T18:32:45.904335", "exception": false, "start_time": "2021-07-29T18:32:44.896380", "status": "completed"} tags=[]
graph = pm.model_to_graphviz(model)
graph.format = "png"
graph.render(pictures_dir / "{}_graph".format(model_id))
graph
```

<!-- #region papermill={"duration": 0.02653, "end_time": "2021-07-29T18:32:45.958699", "exception": false, "start_time": "2021-07-29T18:32:45.932169", "status": "completed"} tags=[] -->
A prior predictive sampling can help to check if the model works correctly.
<!-- #endregion -->

```python papermill={"duration": 1.051526, "end_time": "2021-07-29T18:32:47.038386", "exception": false, "start_time": "2021-07-29T18:32:45.986860", "status": "completed"} tags=[]
with model:
    prior = pm.sample_prior_predictive(samples=100)
```

<!-- #region papermill={"duration": 0.027291, "end_time": "2021-07-29T18:32:47.092874", "exception": false, "start_time": "2021-07-29T18:32:47.065583", "status": "completed"} tags=[] -->
## Inference of model parameters

We solve the inference problem with NUTS algorithm after an initialization using ADVI.
<!-- #endregion -->

```python papermill={"duration": 11531.124155, "end_time": "2021-07-29T21:44:58.243333", "exception": false, "start_time": "2021-07-29T18:32:47.119178", "status": "completed"} tags=[]
if run_inference:
    with model:
        pbar = Progress() # progress bar for logging with papermill
        trace = pm.sample(target_accept=0.85, return_inferencedata=False, init="advi", chains=4, draws=600, tune=200, cores=4,
                         callback=pbar.do_update, random_seed=42)
        idata = az.from_pymc3(trace, model=model)
```

```python papermill={"duration": 34.870101, "end_time": "2021-07-29T21:45:33.174464", "exception": false, "start_time": "2021-07-29T21:44:58.304363", "status": "completed"} tags=[]
saved_data_file = data_dir / "{}_data.nc".format(model_id)
if run_inference:
    az.to_netcdf(idata, saved_data_file)
else:
    idata = az.from_netcdf(saved_data_file)
```

<!-- #region papermill={"duration": 0.037491, "end_time": "2021-07-29T21:45:33.250552", "exception": false, "start_time": "2021-07-29T21:45:33.213061", "status": "completed"} tags=[] -->
## Convergence assessment

To check the convergence quality of the inference process, we report the following statistics:

- $MCSE$: Monte Carlo standard error. Must be as low as possible.
- $ESS$: Effective Sample size. Low values for some parameter indicate poor sampling performance (higher autocorrelation between samples).
- $\hat{R}$: Indicator of similarity between Monte Carlo sampling chains. Must be comprised between 1 and 1.1
- NUTS sampling above should have (or only very few) divergent transitions.
<!-- #endregion -->

```python papermill={"duration": 0.604897, "end_time": "2021-07-29T21:45:33.892626", "exception": false, "start_time": "2021-07-29T21:45:33.287729", "status": "completed"} tags=[]
az.summary(idata, kind="diagnostics").transpose()
```

```python papermill={"duration": 0.625495, "end_time": "2021-07-29T21:45:34.556922", "exception": false, "start_time": "2021-07-29T21:45:33.931427", "status": "completed"} tags=[]
## Sampling stats summary
stats_table = az.summary(idata, kind="diagnostics").describe().drop(axis=0, labels=["count", "25%", "50%", "75%"])
stats_table.to_latex(tables_dir / "{}_stats_table.tex".format(model_id), float_format='%.3g')
stats_table
```

```python papermill={"duration": 0.557852, "end_time": "2021-07-29T21:45:35.153940", "exception": false, "start_time": "2021-07-29T21:45:34.596088", "status": "completed"} tags=[]
# mu effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["sigma"]).loc["ess_bulk",:].query("mu < 600").sort_values("mu").set_index("categories").rename(columns={"mu": "mu: effective sample size"})
```

```python papermill={"duration": 0.557852, "end_time": "2021-07-29T21:45:35.153940", "exception": false, "start_time": "2021-07-29T21:45:34.596088", "status": "completed"} tags=[]
# mu effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["sigma"]).loc["ess_tail",:].query("mu < 600").sort_values("mu").set_index("categories").rename(columns={"mu": "mu: effective sample size"})
```

```python papermill={"duration": 0.562892, "end_time": "2021-07-29T21:45:35.757293", "exception": false, "start_time": "2021-07-29T21:45:35.194401", "status": "completed"} tags=[]
# sigma effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["mu"]).loc["ess_bulk",:].sort_values("sigma").query("sigma < 600").set_index("categories").rename(columns={"sigma": "sigma: effective sample size"})
```

```python papermill={"duration": 0.562892, "end_time": "2021-07-29T21:45:35.757293", "exception": false, "start_time": "2021-07-29T21:45:35.194401", "status": "completed"} tags=[]
# sigma effective sample size
az.summary(idata, kind="diagnostics", fmt="long").drop(columns=["mu"]).loc["ess_tail",:].query("sigma < 600").sort_values("sigma").set_index("categories").rename(columns={"sigma": "sigma: effective sample size"})
```

```python papermill={"duration": 0.641582, "end_time": "2021-07-29T21:45:36.440861", "exception": false, "start_time": "2021-07-29T21:45:35.799279", "status": "completed"} tags=[]
az.summary(idata, fmt="long").transpose()
```

<!-- #region papermill={"duration": 0.044974, "end_time": "2021-07-29T21:45:36.528786", "exception": false, "start_time": "2021-07-29T21:45:36.483812", "status": "completed"} tags=[] -->
We can visually evaluate the quality of sampling by examining its regularity in the trace plot below.
<!-- #endregion -->

```python papermill={"duration": 10.933283, "end_time": "2021-07-29T21:45:47.505294", "exception": false, "start_time": "2021-07-29T21:45:36.572011", "status": "completed"} tags=[]
az.plot_trace(idata, compact=True, divergences=None)
plt.savefig(pictures_dir / "{}_trace".format(model_id))
```

<!-- #region papermill={"duration": 0.048668, "end_time": "2021-07-29T21:45:47.602737", "exception": false, "start_time": "2021-07-29T21:45:47.554069", "status": "completed"} tags=[] -->
Besides, posterior predictive sampling of output variables on training and validation datasets must match as much as possible observed distributions.
<!-- #endregion -->

```python papermill={"duration": 76.161867, "end_time": "2021-07-29T21:47:03.811887", "exception": false, "start_time": "2021-07-29T21:45:47.650020", "status": "completed"} tags=[]
# sample posterior predictive
if run_inference:
    with model:
        ppc_fit = pm.sample_posterior_predictive(trace, samples=500, random_seed=42)
        idata_post_pred = az.from_pymc3(trace=trace, posterior_predictive=ppc_fit)
    az.to_netcdf(idata_post_pred, data_dir / "{}_idata_post_pred.nc".format(model_id))
else:
    idata_post_pred = az.from_netcdf(data_dir / "{}_idata_post_pred.nc".format(model_id))
```

```python papermill={"duration": 26.739576, "end_time": "2021-07-29T21:47:30.602470", "exception": false, "start_time": "2021-07-29T21:47:03.862894", "status": "completed"} tags=[]
pc_plot = az.plot_ppc(idata_post_pred, figsize=(12,4), coords={"variables": ["hlc_avg","base_consumption_avg", "Ts"]}, flatten=["iris"], mean=False)
pc_plot[0].set_xlim([0,5])
pc_plot[1].set_xlim([0,10])
plt.suptitle("Posterior predictive distributions for training data", fontsize=16)
plt.savefig(pictures_dir / "{}_ppc_training".format(model_id))
```

```python papermill={"duration": 27.818314, "end_time": "2021-07-29T21:47:58.476841", "exception": false, "start_time": "2021-07-29T21:47:30.658527", "status": "completed"} tags=[]
# sample posterior predictive on validation dataset
if run_inference:
    with model:
        model.set_data_prediction()
        ppc_valid = pm.sample_posterior_predictive(trace, samples=500, random_seed=42)
        idata_post_valid = az.from_pymc3(trace=trace, posterior_predictive=ppc_valid)
    az.to_netcdf(idata_post_valid, data_dir / "{}_idata_post_valid.nc".format(model_id))
else:
    idata_post_valid = az.from_netcdf(data_dir / "{}_idata_post_valid.nc".format(model_id))
```

```python papermill={"duration": 26.542526, "end_time": "2021-07-29T21:48:25.077365", "exception": false, "start_time": "2021-07-29T21:47:58.534839", "status": "completed"} tags=[]
ppc_plot = az.plot_ppc(idata_post_valid, figsize=(12,4), coords={"variables": ["hlc_avg","base_consumption_avg", "Ts"]}, flatten=["iris"], mean=False)
ppc_plot[0].set_xlim([0,5])
ppc_plot[1].set_xlim([0,10])
plt.suptitle("Posterior predictive distributions for validation data", fontsize=16)
plt.savefig(pictures_dir / "{}_ppc_validation".format(model_id))
```

<!-- #region papermill={"duration": 0.058494, "end_time": "2021-07-29T21:48:25.192486", "exception": false, "start_time": "2021-07-29T21:48:25.133992", "status": "completed"} tags=[] -->
## Results interpretation

To physically interpret inferred parameters, we can compare them on a tree plot with teir confidence intervals.
<!-- #endregion -->

```python papermill={"duration": 2.284613, "end_time": "2021-07-29T21:48:27.534247", "exception": false, "start_time": "2021-07-29T21:48:25.249634", "status": "completed"} tags=[]
variables = ["hlc_avg", "base_consumption_avg", "Ts"]
x_labels = ["dwelling HLC (kWh/K)", "dwelling base consumption (MWh)", "dwelling Ts (°C)"]
var_colors = ["blue", "red",  "teal"]
params = ["mu", "sigma"]
fig, ax = plt.subplots(len(params), len(variables), gridspec_kw={'height_ratios': [1, 1]})
fig.set_figheight(max(5, len(labels)*10/12))
for i, p in enumerate(params):
    for j, v in enumerate(variables):
        axis = ax[i][j]
        az.plot_forest(idata, kind="forestplot", coords={"variables":v}, var_names=p, combined=True, ax=axis, colors=var_colors[j])
        if i > 0:
            axis.set_title("")
        if i == len(params) - 1:
            axis.set_xlabel(x_labels[j], fontsize="x-large")
        if j > 0:
            axis.get_yaxis().set_visible(False)
plt.tight_layout()
plt.savefig(pictures_dir / "{}_treeplot".format(model_id))
```

```python papermill={"duration": 0.060452, "end_time": "2021-07-29T21:48:27.656110", "exception": false, "start_time": "2021-07-29T21:48:27.595658", "status": "completed"} tags=[]

```
