---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Preliminary data analysis for the residential French stock thermo-energetic data

```python
## imports
# local package
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp
# science packs
import numpy as np
import pandas as pd
# graphing packs
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import rcParams
from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sns
# utilities
from pathlib import Path
# figure parameters
FIGSIZE = (12, 6)
rcParams['figure.figsize'] = FIGSIZE
rcParams["savefig.dpi"] = 250
rcParams["savefig.format"] = "png"
```

```python
# file paths
nb_dir = "/home/nils/Documents/ProjetsDev/bayesian_archetypes/notebooks"
proj_dir = Path(nb_dir).parents[0]
data_dir = proj_dir / "data" / "inference" / "local"
pictures_dir = proj_dir / "notebooks" / "pictures"
tables_dir  = proj_dir / "notebooks" / "tables"
print(proj_dir)
```

## Data setup

First, we setup and preprocess the available data.
The data is preprocessed from raw data sources in tabular form where each row matches one IRIS (specific geographic area of metropolitan France of roughly 500 to 2000 dwellings):

- ratios for dwellings categories
- number of dwellings per IRIS
- yearly aggregated energy signature parameters:
    - $hlc_{avg}$, the average heat loss coefficient per IRIS
    - ${base\ consumption}_{avg}$, the average consumption not due to temperature variations
    - $T_s$, the temperature switch point

```python
### setup data
data = io.Data()
iris = cp.Iris(data)

dwellings_label = "enedis_sites_number"

iris.thermo_df["hlc_avg"] = iris.thermo_df["hlc"] / iris.thermo_df[dwellings_label]
iris.thermo_df["base_consumption_avg"] = iris.thermo_df["base_consumption"] / iris.thermo_df[dwellings_label]

regions_df = data.france_metro_df()
```

```python
df = iris.borders_df.join(iris.thermo_df)
```

```python
100 * len(iris.thermo_df) / len(data.insee_df())
```

Not all IRISes have available thermo-energetic data (42% of IRISes).  
The choropleth plot below shows apartment ratios for viable IRISes over metropolitan France.

```python
# ratio of apartments per used IRIS
ax = df.plot(column="ratio_apartment", figsize=(6,6), legend=True)
regions_df.boundary.plot(ax=ax, color="orange")
```

Likewise, we can plot maps for average energy signature values :
- $hlc_{avg}$
- ${base\ consumption}_{avg}$
- $T_s$

```python
# apartment ratios, base consumption, HLC, TS choromap
titles = ["(a) Apartments ratio (%/100) per IRIS", "(b) Average base consumption per dwelling (MWh)", "(c) Average hlc per dwelling (kWh/°C)", "(d) Ts temperatures per IRIS (°C)"]
variables = ["ratio_apartment", "base_consumption_avg", "hlc_avg", "Ts"]

norms = [mpl.colors.Normalize(vmin=0, vmax=1),
         mpl.colors.Normalize(vmin=1, vmax=6),
         mpl.colors.Normalize(vmin=0, vmax=3),
         mpl.colors.Normalize(vmin=10, vmax=19)]

cmaps = ["viridis", "cividis", "plasma", "inferno"]

fig, axs = plt.subplots(2, 2, figsize=(12,12))
for i, ax in enumerate(fig.axes):
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    df.plot(column=variables[i], ax=ax, legend=True, cax=cax, cmap=cmaps[i], norm=norms[i])
    regions_df.boundary.plot(ax=ax, color="black", linewidth=0.5)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.tick_params(labelsize=12)
    ax.set_title(titles[i], fontweight="bold", fontsize=12)
plt.tight_layout()
plt.savefig(pictures_dir / "iris_choropleths")
```

These values have positive distributions over all IRISes:

```python
# Aggregated distributions of main variables at national scale
titles = ["(a) Average base consumption (MWh)", "(b) Average hlc (kWh/°C)", "(c) Ts temperatures (°C)"]
xlabels = ["avg. base consumption (MWh)", "avg. hlc (kWh/°C)", "Ts temperature (°C)"]
variables = ["base_consumption_avg", "hlc_avg", "Ts"]
fig, axes = plt.subplots(1, 3, figsize=(12,3), sharey=True)
for i, ax in enumerate(fig.axes):
    sns.histplot(df[variables[i]], kde=True, ax=ax, bins=50)
    ax.set_title(titles[i], fontweight="bold", fontsize=12)
    ax.set_xlabel(xlabels[i])
plt.tight_layout()
axes[0].set_xlim([0,7])
axes[1].set_xlim([0,3])
axes[2].set_xlim([12.5,19])
plt.savefig(pictures_dir / "energy_signature_distributions")
```

```python
sns.pairplot(df[variables])
```

Likewise, we can observe census data for dwelling catedories:

```python
# census data
ratio_cols = [c for c in iris.thermo_df.columns if "ratio_" in c]
ratios = iris.thermo_df[ratio_cols]
census = ratios * iris.thermo_df[["dwellings_number"]].values
```

```python
surf = [c for c in ratio_cols if "surf" in c]
date = [c for c in ratio_cols if "date" in c]
date.insert(6, 'ratio_date_house_2013_inf')
date.pop(-2)
date_house = [c for c in date if "house" in c]
date_apart = [c for c in date if "apart" in c]
```

```python
x_labels = [
    [l.split("surf_")[1].replace("_"," ") for l in surf],
    [l.split("house_")[1].replace("_"," ") for l in date_house],
    [l.split("apartment_")[1].replace("_"," ") for l in date_apart]
]
titles = ["surface categories", "house date categories", "apartment date categories"]
cols_list = [surf, date_house, date_apart]
fig, axes = plt.subplots(1, 3, figsize=(12,3), sharey=True)
for i, ax in enumerate(fig.axes):
    dfp = census[cols_list[i]]
    dfp.columns = x_labels[i]
    (dfp.sum() / 1e6).plot.bar(ax=ax, color="teal", rot=-45)
    ax.set_title(titles[i], fontweight="bold")
axes[0].set_ylabel("count [millions]")
plt.savefig(pictures_dir / "census_distributions",  bbox_inches = 'tight')
```
