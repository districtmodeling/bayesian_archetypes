# Comparison of TABULA - Episcope French Average Buildings and stock study

The [TABULA - Episcope projects](https://episcope.eu/welcome/) aim to study European building stocks and introduce building archetypes ("Average Buildings") for studied countries.
In this notebook, we extract Heat Loss Coefficients (HLC) from average buildings defined in [TABULA WEBTOOL](https://webtool.building-typology.eu) by retreiving thermal data from API endpoints. 

Then we compare extracted values with infered HLC distributions for corresponding categories.

Eventually, we use a simple Monte-Carlo approach to simulate the TABULA French buildings stock HLC (about 10331 dwellings) and compare the resulting values with TABULA projected value.

```python
## imports
# local package
import bayesian_archetypes.io as io
import bayesian_archetypes.compute as cp
import bayesian_archetypes.tabula as tb
# science packs
import pymc3 as pm
import numpy as np
import pandas as pd
import theano.tensor as tt
# graphing packs
import matplotlib.lines as mlines
import arviz as az
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
# utilities
import os
from pathlib import Path
tp = tt.printing.Print
# figure parameters
FIGSIZE = (12, 6)
rcParams['figure.figsize'] = FIGSIZE
rcParams["savefig.dpi"] = 250
rcParams["savefig.format"] = "png"
```

```python
nb_dir = os.path.dirname(os.path.realpath("__file__"))
```

```python
# file paths
proj_dir = Path(nb_dir).parents[0]
data_dir = proj_dir / "data" / "inference" / "local"
pictures_dir = proj_dir / "notebooks" / "pictures"
tables_dir  = proj_dir / "notebooks" / "tables"
print(proj_dir)
```

TABULA data is extracted from API endpoints and set up in a DataFrame.
This data provides the following variables:

- $year1\_building$ : construction year of the building (or first year of the construction period)
- $year2\_building$ : construction year of the building (or last year of the construction period)
- $code\_buildingsizeclass$ : building typology (single family house, multi terraced house ...)
- $a\_c\_ref$ : energy reference area (conditioned floor area, internal dimensions)
- $n\_apartment$ : number of apartments
- $heatingdays$ : number of days per year during heating season (average daily temperature is below or equal the base temperature)
- $theta\_e$ : average external air temperature during the heating season
- $theta\_i$: internal temperature
- $sum\_deltat\_for\_heatingdays$ : accumulated difference between internal and external temperature
- $q\_h\_nd$ : energy need for heating per area for one building

Then HLC per dwelling is estimated by the following formula (included in the returned DataFrame):

$${HLC}_{dwelling} = \frac{{q\_h\_nd}.{a\_c\_ref}}{{sum\_deltat\_for\_heatingdays}.{n\_apartment}}$$

```python
tabula = tb.TabulaAPI()
tabula_df = tabula.french_buildings_df
tabula_df.describe()
```

Building classes are a bit different from those used in previous inference. We must perform conversion of TABULA classes towards classes in IRIS surveys.

```python
# type, date, surf
def dwelling_type(x):
    # input : "n_apartment"
    t = "house"
    if x > 1:
        t = "apartment"
    return t

def date_category(x):
    # input : "year2_building"
    date = None
    d2 = x
    if d2 < 1919:
        date = "inf_1919"
    elif d2 < 1950:
        date = "1919_1945"
    elif d2 < 1970:
        date = "1945_1970"
    elif d2 < 1990:
        date = "1970_1990"
    elif d2 < 2006:
        date = "1990_2005"
    elif d2 < 2013:
        date = "2005_2013"
    else:
        date = "2013_inf"
    return date

def surface_category(x):
    # input : "mean_area"
    s = x
    surface = None
    if s < 31:
        surface = "inf_30"
    elif s < 41:
        surface = "30_40"
    elif s < 61:
        surface = "40_60"
    elif s < 81:
        surface = "60_80"
    elif s < 101:
        surface = "80_100"
    elif s < 121:
        surface = "100_120"
    else:
        surface = "120_inf"
    return surface

tabula_df["mean_area"] = tabula_df["a_c_ref"] / tabula_df["n_apartment"]
tabula_df["type"] = tabula_df["n_apartment"].apply(dwelling_type)
tabula_df["surface"] = tabula_df["mean_area"].apply(surface_category)
tabula_df["date"] = tabula_df["year2_building"].apply(date_category)
tabula_df["type_date"] = tabula_df["type"] + "_" + tabula_df["date"] 
```

HLC distribution parameters are retreived from inference data results :

```python
res_type = az.summary(az.from_netcdf(data_dir / "types_data.nc"), fmt="long").loc["mean"].query("variables == 'hlc_avg'")
res_type
```

```python
res_surf = az.summary(az.from_netcdf(data_dir / "surfaces_data.nc"), fmt="long").loc["mean"].query("variables == 'hlc_avg'")
res_surf["categories"] = [c.split("surf_")[1] for c in res_surf["categories"]]
res_surf
```

```python
res_dates = az.summary(az.from_netcdf(data_dir / "dates_data.nc"), fmt="long").loc["mean"].query("variables == 'hlc_avg'")
res_dates["categories"] = [c.split("date_")[1] for c in res_dates["categories"]]
res_dates
```

For types, surfaces and type-dates results, we draw the resulting distributions and display corresponding average buildings from TABULA. Average buildings are provided in 3 variants: no refurbishment, usual refurbishment and advanced refurbishment.

```python
hlc_sim_type = pd.DataFrame(columns=["hlc", "type"])
for r in res_type.iterrows():
    cat = r[1]["categories"]
    mu = r[1]["mu"]
    sigma = r[1]["sigma"]
    samples = pm.Gamma.dist(mu=mu, sigma=sigma, shape=5000).random()
    samples_df = pd.DataFrame(samples, columns=["hlc"])
    samples_df["type"] = cat
    hlc_sim_type = hlc_sim_type.append(samples_df)
```

```python
no_refurb = mlines.Line2D([], [], linewidth=0, color='red', marker='.', markersize=15, label='no refurbishment')
refurb_1 = mlines.Line2D([], [], linewidth=0, color='orange', marker='.', markersize=15, label='usual refurbishment')
refurb_2 = mlines.Line2D([], [], linewidth=0, color='green', marker='.', markersize=15, label='advanced refurbishment')
legend_handles = [no_refurb, refurb_1, refurb_2]
```

```python
sns.boxplot(data=hlc_sim_type, x="hlc", y="type", showfliers = False)
sns.swarmplot(data=tabula_df.query("refurbishment_id == 1"), y="type", x="hlc_kwh_k", color="red")
sns.swarmplot(data=tabula_df.query("refurbishment_id == 2"), y="type", x="hlc_kwh_k", color="orange")
sns.swarmplot(data=tabula_df.query("refurbishment_id == 3"), y="type", x="hlc_kwh_k", color="green")
plt.legend(handles=legend_handles)
plt.xlabel("dwelling hlc (kWh/K)")
plt.savefig(pictures_dir / "comparison_tabula_type")
```

```python
hlc_sim_surf = pd.DataFrame(columns=["hlc", "surface"])
for r in res_surf.iterrows():
    cat = r[1]["categories"]
    mu = r[1]["mu"]
    sigma = r[1]["sigma"]
    samples = pm.Gamma.dist(mu=mu, sigma=sigma, shape=5000).random()
    samples_df = pd.DataFrame(samples, columns=["hlc"])
    samples_df["surface"] = cat
    hlc_sim_surf = hlc_sim_surf.append(samples_df)
```

```python
y_labels = res_surf.categories.to_list()[2:]
ax = sns.boxplot(data=hlc_sim_surf, x="hlc", y="surface", order=y_labels, showfliers = False)
sns.swarmplot(ax=ax, data=tabula_df.query("refurbishment_id == 1"), y="surface", x="hlc_kwh_k", color="red", order=y_labels)
sns.swarmplot(ax=ax, data=tabula_df.query("refurbishment_id == 2"), y="surface", x="hlc_kwh_k", color="orange", order=y_labels)
sns.swarmplot(ax=ax, data=tabula_df.query("refurbishment_id == 3"), y="surface", x="hlc_kwh_k", color="green", order=y_labels)
plt.legend(handles=legend_handles)
plt.xlabel("dwelling hlc (kWh/K)")
plt.savefig(pictures_dir / "comparison_tabula_surface")
```

```python
hlc_sim_date = pd.DataFrame(columns=["hlc", "type_date"])
for r in res_dates.iterrows():
    cat = r[1]["categories"]
    mu = r[1]["mu"]
    sigma = r[1]["sigma"]
    samples = pm.Gamma.dist(mu=mu, sigma=sigma, shape=5000).random()
    samples_df = pd.DataFrame(samples, columns=["hlc"])
    samples_df["type_date"] = cat
    hlc_sim_date = hlc_sim_date.append(samples_df)
```

```python
y_labels = res_dates.categories.to_list()
sns.boxplot(data=hlc_sim_date, x="hlc", y="type_date", order=y_labels, showfliers = False)
sns.swarmplot(data=tabula_df.query("refurbishment_id == 1"), y="type_date", x="hlc_kwh_k", color="red", order=y_labels)
sns.swarmplot(data=tabula_df.query("refurbishment_id == 2"), y="type_date", x="hlc_kwh_k", color="orange", order=y_labels)
sns.swarmplot(data=tabula_df.query("refurbishment_id == 3"), y="type_date", x="hlc_kwh_k", color="green", order=y_labels)
#plt.xlim([0,12])
plt.legend(handles=legend_handles)
plt.xlabel("dwelling hlc (kWh/K)")
plt.savefig(pictures_dir / "comparison_tabula_date_type")
```

## Simulate French TABULA building stock


French studies for TABULA project were performed by the company "Pouget Consultants" over a stock of 10331 dwellings located in the city of Montreuil (Paris suburbs). Study results provides an estimate of net energy needed for heating of 54353 MWh/year.
Categories of dwellings in the stock are not very clearly referenced, but we can make resoneable assumptions of type (all apartments) and construction dates by merging information between the [study report](https://episcope.eu/fileadmin/episcope/public/docs/pilot_actions/FR_EPISCOPE_LocalCaseStudy_Pouget.pdf) and [calculus tables](https://s2.building-typology.eu/abpdf/FR_L_01_EPISCOPE_CaseStudy_TABULA_Local.pdf).

From such assumptions, we can exploit infered categories distributions for HLC to perform a simple Monte-Carlo estimation of the stock HLC: 
- Given the rough construction date range, we create an interpolated distribution to draw construction dates.
- For each category of drawn dates, we draw the corresponding number of hlc values for the corresponding distribution. 
- Summing-up these HLC values gives an estimate for the whole stock. We repeat the process 5000 times to estimate a representative distribution.

```python
## categories for building stock :
# LC => Apartment
# dates => inf-48, 49-74, 75-99, 2000-inf
```

```python
def compute_hlc_stock_by_date():
    date_points = np.array([1915,1945,1955,1975,1980,1995,2005,2012])
    n_total = 10331
    p_points = np.array([504, 504, 4953, 4953, 4446, 4446, 428, 428]) / n_total
    dates = pm.Interpolated.dist(x_points=date_points, pdf_points=p_points, shape=int(n_total)).random()
    date_categories = dates.astype('U256')
    date_categories[:] = "apartment_2013_inf"
    date_categories[dates < 2013] = "apartment_2005_2013"
    date_categories[dates < 2005] = "apartment_1990_2005"
    date_categories[dates < 1990] = "apartment_1970_1990"
    date_categories[dates < 1970] = "apartment_1945_1970"
    date_categories[dates < 1945] = "apartment_1919_1945"
    date_categories[dates < 1919] = "apartment_inf_1919"
    categories, counts = np.unique(date_categories, return_counts=True)
    hlc_stock = np.array([])
    for i, cat in enumerate(categories):
        mu = res_dates.set_index("categories").loc[cat].mu
        sigma = res_dates.set_index("categories").loc[cat].sigma
        hlc_samples = pm.Gamma.dist(mu=mu, sigma=sigma, shape=counts[i]).random()
        hlc_stock = np.hstack([hlc_stock, hlc_samples])
    return hlc_stock.sum()
```

```python
from tqdm.notebook import tqdm
hlc_stock_samples = np.array([])
for i in tqdm(range(5000)):
    hlc_stock_samples = np.append(hlc_stock_samples, compute_hlc_stock_by_date())
```

```python
hlc_stock_samples.mean()
```

```python
hlc_stock_samples.std()
```

The stock heat need is estimated in TABULA report to 54353 MWh/year, with 2885K degree-days (DJU). This results in an approximation of 18839 kWh/K for the coresponding HLC. This is almost 4 times bigger than our Monte-Carlo simulation. This can be explained by the high value of HLC in average buildings without refurbishment, in comparison with our infered values.

```python
DJU = 2885 #Kelvins
heat_need = 54353 * 1000 #kWh/a
tabula_stock_hlc = heat_need / DJU #KWh
tabula_stock_hlc
```

```python
tabula_stock_hlc / 4950 # ==> stock estimation about 4 times bigger
```

For further comparison, a simple cross product over the city of Montreuil with ENEDIS consumption data gives a result closer to our Monte-Carlo simulation (6200 kWh/K).

```python
data = io.Data()
iris = cp.Iris(data)
conso_df = data.enedis_df()
montreuil_iris_codes = conso_df[conso_df["Nom commune"] == "Montreuil"]["Code IRIS"].to_list()
n_dwellings_montreuil = iris.thermo_df.reindex(montreuil_iris_codes)["dwellings_number"].sum()
hlc_montreuil = iris.thermo_df.reindex(montreuil_iris_codes)["hlc"].sum()
hlc_cross_product = 10331 * hlc_montreuil / n_dwellings_montreuil # kWh/K
hlc_cross_product 
```

```python
hlc_stock_results_df = pd.DataFrame.from_dict(data={"MC estimate: mean": hlc_stock_samples.mean(), "MC estimate: std": hlc_stock_samples.std(), "TABULA": tabula_stock_hlc, "ENEDIS cross-product": hlc_cross_product}, columns=["Stock hlc (kWh/K)"], orient="index")
hlc_stock_results_df
```

```python
table = hlc_stock_results_df.round(0).astype(int)
sns.histplot(hlc_stock_samples, kde=True)
plt.table(cellText=table.values, rowLabels=table.index, colLabels=table.columns, bbox=[0.8,0.7,0.2,0.3])
plt.xlabel("stock HLC (kWh/K)")
plt.xlim([4650, 5100])
plt.savefig(pictures_dir / "comparison_mc_tabula_stock",  bbox_inches = 'tight')
```
