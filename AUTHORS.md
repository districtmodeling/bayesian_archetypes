# Contributors

- Nils Artiges, Univ. Grenoble Alpes, CNRS, Grenoble INP (Institute of Engineering Univ. Grenoble Alpes), G2Elab, F-38000 Grenoble, France ; <nils DOT artiges AT g2elab.grenoble-inp.fr>
- Benoit Delinchant, Univ. Grenoble Alpes, CNRS, Grenoble INP\footnote{Institute of Engineering Univ. Grenoble Alpes}, G2Elab, F-38000 Grenoble, France ; <benoit DOT delinchant AT g2elab.grenoble-inp.fr>
- Simon Rouchier, Université Savoie Mont-Blanc, LOCIE ; <simon DOT rouchier AT univ-smb.fr>

# Acknowledgements

This work is supported by the French National Research Agency in the framework of the *investissements d’avenir program* (ANR-15-IDEX-0002) through the **Eco-SESA cross-disciplinary project**, by *La Région Auverge Rhone-Alpes* through the **Orebe** project, by ADEME (Agence de l'environnement et de la maîtrise de l'énergie) through the **Rethine** project and by CARNOT through the **Solpreca** project.

