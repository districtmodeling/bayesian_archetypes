# bayesian archetypes

Energy signature inference from national data for statistical definition of buildings archetypes. This project contains the code and data used for the paper [Bayesian inference of dwellings energy signature at national scale: case of the French residential stock](https://www.mdpi.com/1262982) published in MDPI Energies.  
Full documentation with notebook results is available [HERE](https://districtmodeling.gricad-pages.univ-grenoble-alpes.fr/bayesian_archetypes/).

<img src="logos/logo_UGA_couleur_rvb.png" height="100" />
<img src="logos/g2elab_transparent_0.png" height="100" />
<img src="logos/locie-logo-1.png" height="100" />

## Citing

If you reuse all or part of this code and results or want to refer to this work, please cite this repository though Zenodo and the corresponding research paper:
- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5142591.svg)](https://doi.org/10.5281/zenodo.5142591)
- [Bayesian inference of dwellings energy signature at national scale: case of the French residential stock](https://www.mdpi.com/1262982)

## Hardware and OS

This code is tested and runs on a Intel-Core i5-8250U CPU (1.6-1.8 GHz) laptop with 11 Go of RAM under Ubuntu 20.04 LTS.

## Installation

First, you can download the full repository directly from its *gitlab* page or with a `git clone`. To download all data with `git clone`, please ensure you have installed [git lfs](https://git-lfs.github.com/).

This project relies on `pymc3` for Bayesian Inference implementation.
It also uses `pandas`, `geopandas`, `pytables` and other open source python packages.  
`arviz` and `seaborn` are used for plotting, `black` and `pytest` for formatting and unit testing. The report generation scripts rely on `jupytext` and `papermill`.

To install all requirements, the easiest way is to setup a `conda` environnement from the `environment.yml` file in the project folder.

```console
conda env create -f environment.yml
conda activate bayesian
python setup.py develop
```

Alternatively, we can install packages listed in `environment.yml` manually with `conda forge` as installation channel.

```console
conda create --name bayesian python=3.9
conda activate bayesian

conda install -c conda-forge pymc3=3.11.2 
conda install -c conda-forge geopandas=0.9.0
conda install -c conda-forge seaborn=0.11.1 
conda install -c conda-forge pytables seaborn jupyter black pytest
conda install -c conda-forge ... 

python setup.py develop
```

## How to use

Once the project is downloaded and your `python` virtual environnement properly set-up, you are ready to execute Jupyter notebooks provided in the [notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/notebooks) folder.

These notebooks are provided in Markdown format to ease versioning and must be opened in Jupyter using the `jupytext` extension. Current results of execution are provided in [notebooks/reports](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/notebooks/reports).

Main results of the paper are related with [building_signature_inference.md](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/notebooks/building_signature_inference.md). Other notebooks as provided as complementary or experimental material and are less consolidated.

All data handling code is located in [src/bayesian_archetypes](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/src/bayesian_archetypes) package. This package is imported in each notebook. On a first run, it will perform all data preprocessing and save it in [data/preprocessed](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/data/preprocessed). This operation may take about 15 minutes and will be only performed once, unless you delete preprocessed data files.

Original data files are located in [data/raw](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/data/raw). Inference data (avoiding a full model inference if not necessary), are stored by notebooks in [data/inference](https://gricad-gitlab.univ-grenoble-alpes.fr/districtmodeling/bayesian_archetypes/-/tree/master/data/inference).

## Contributing

If you want to contribute or if you identified some problems, you may raise an issue directly on the `gitlab` repository.

## Acknowledgments
This work is supported by the French National Research Agency in the framework of the *investissements d’avenir program* (ANR-15-IDEX-0002) through the **Eco-SESA cross-disciplinary project**, by *La Région Auverge Rhone-Alpes* through the **Orebe** project, by ADEME (Agence de l'environnement et de la maîtrise de l'énergie) through the **Rethine** project and by CARNOT through the **Solpreca** project.

