.. _notebooks:

=========
Notebooks
=========
-------------------------
Preliminary data analysis
-------------------------
.. raw:: html
   :file: ../notebooks/reports/preliminary_data_analysis.html

---------------------------------------------
Energy signature inference for dwelling types
---------------------------------------------
.. raw:: html
   :file: ../notebooks/reports/building_signature_inference_types.html

------------------------------------------------
Energy signature inference for dwelling surfaces
------------------------------------------------
.. raw:: html
   :file: ../notebooks/reports/building_signature_inference_surfaces.html

---------------------------------------------
Energy signature inference for dwelling dates
---------------------------------------------
.. raw:: html
   :file: ../notebooks/reports/building_signature_inference_dates.html

--------------------------------------------------
HLC comparisons between inferred and TABULA values
--------------------------------------------------
.. raw:: html
   :file: ../notebooks/reports/tabula_hlc_comparison.html

----------------------------
Population Inference (trial)
----------------------------
.. raw:: html
   :file: ../notebooks/reports/population_inference.html

---------------------
Mixture model (trial)
---------------------
.. raw:: html
   :file: ../notebooks/reports/building_signature_inference_mixture.html


