from setuptools import find_packages
from setuptools import setup
from glob import glob

setup(
    name="bayesian_archetypes",
    version="0.1.1",
    author="Nils Artiges",
    author_email="nils.artiges@g2elab.grenoble-inp.fr",
    description=(
        "Energy signature inference from national data for statistical definition of buildings archetypes"
    ),
    license="Apache2.0",
    packages=find_packages("src"),
    package_dir={"": "src"},
    py_modules=[splitext(basename(path))[0] for path in glob("src/*.py")],
    include_package_data=True,
)
